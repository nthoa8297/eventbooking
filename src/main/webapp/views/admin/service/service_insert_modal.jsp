<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">          
            <form id="my-form" novalidate action="/Ebms/Service/Insert">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm dịch vụ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Tên dịch vụ</label>
                            <input name="name"   class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Giá</label>
                            <input type="number" min="0" name="price" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Nhà cung cấp</label>
                            <select required class="form-control" name="providerID" data-type="int" data-json="id">
                                <option>-Lựa chọn nhà cung cấp-</option>
                                <c:forEach items="${LIST_PROVIDER}" var="item">
                                    <option value="${item.id}">${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nội dung</label>
                            <textarea class="form-control" name="description" rows="7"></textarea>
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Huỷ</button>
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>      
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <script>
        $(document).ready(function () {
            $('#my-form').validate({
                submitHandler: function () {
                    ;
                    var data = $('#my-form').serializeObject();
                    var url = $('#my-form').attr('action');
                    console.log(data);
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function (result) {
                            openAlert(result.value);
                            if (result.key) {
                                $('#myModal').modal('hide');
                                reloadAjaxContent();
                            }
                        },
                        error: function () {
                            errorAlert();
                        }
                    });
                }
            });
        });
    </script>
</div>