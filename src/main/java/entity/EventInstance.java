package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "EventInstance")
public class EventInstance implements Serializable {

    private Integer id;
    private Address addressID;
    private Event eventID;
    private Date startDate;
    private Date endDate;
    private Integer status;//0: Đã tạo, 1: Đã kết thúc, 2: Đã bị huỷ
    private Date createdDate;
    private String title;
    private String nameAscii;
    private String description;
    private String urlAvatar;
    private List<ServiceMapping> serviceMappings;
//    private Set<Ticket> tickets;
    private Integer vipTicket;
    private Integer normalTicket;
    private BigDecimal ticketPrice;

    @Id
    @Column(name = "ID", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AddressID")
    public Address getAddressID() {
        return addressID;
    }

    public void setAddressID(Address addressID) {
        this.addressID = addressID;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EventID")
    public Event getEventID() {
        return eventID;
    }

    public void setEventID(Event eventID) {
        this.eventID = eventID;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "StartDate")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "EndDate")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Column(name = "Status", insertable = false, updatable = false)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedDate", insertable = false, updatable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "Title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "NameAscii")
    public String getNameAscii() {
        return nameAscii;
    }

    public void setNameAscii(String nameAscii) {
        this.nameAscii = nameAscii;
    }

    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "UrlAvatar")
    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "eventInstanceID")
    @Transient
    public List<ServiceMapping> getServiceMappings() {
        return serviceMappings;
    }

    public void setServiceMappings(List<ServiceMapping> serviceMappings) {
        this.serviceMappings = serviceMappings;
    }

//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "eventInstanceID")
//    public Set<Ticket> getTickets() {
//        return tickets;
//    }
//
//    public void setTickets(Set<Ticket> tickets) {
//        this.tickets = tickets;
//    }

    @Transient
    public Integer getVipTicket() {
        return vipTicket;
    }

    public void setVipTicket(Integer vipTicket) {
        this.vipTicket = vipTicket;
    }

    @Transient
    public Integer getNormalTicket() {
        return normalTicket;
    }

    public void setNormalTicket(Integer normalTicket) {
        this.normalTicket = normalTicket;
    }

    @Transient
    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

}
