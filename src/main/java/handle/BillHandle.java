package handle;

import entity.Bill;
import entity.BillDetail;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

public class BillHandle extends AbstractHandle {

    public BillHandle() {
        super(Bill.class);
    }

    public List paging(Map map) {
        List list = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Bill.class);
                cr.createAlias("adminID", "adminID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("adminID", FetchMode.JOIN);
                cr.createAlias("billDetails", "billDetails", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("billDetails", FetchMode.JOIN);
                cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                Integer totalResult = cr.list().size();
                map.put("totalPage", totalResult % 10 > 0 ? Math.round(totalResult / 10 + 1) : totalResult / 10);
                cr.setFirstResult(((Integer) map.get("currentPage") - 1) * 10);
                cr.setMaxResults(10);
                cr.addOrder(Order.desc("createdDate"));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public Bill findEager(Integer id) {
        Bill room = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Bill.class);
                cr.createAlias("adminID", "adminID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("adminID", FetchMode.JOIN);
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("customerID", FetchMode.JOIN);
                cr.createAlias("billDetails", "billDetails", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("billDetails", FetchMode.JOIN);
                cr.add(Restrictions.eq("id", id));
                room = (Bill) cr.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return room;
    }

    public Bill save(Bill bill) {
        Session session = null;
        Transaction tran = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                tran = session.beginTransaction();
                session.save(bill);
                String ticketIDs = "UPDATE Ticket set IsSold=1 WHERE Id in (0";
                for (BillDetail detail : bill.getBillDetails()) {
                    detail.setBillID(bill);
                    session.save(detail);
                    ticketIDs += "," + detail.getTicketID().getId();
                }
                ticketIDs += ")";
                session.createSQLQuery(ticketIDs).executeUpdate();
                HibernateConfiguration.getInstance().commitTransaction(tran);
            }
        } catch (Exception e) {
            HibernateConfiguration.getInstance().rollbackTransaction(tran);
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return bill;
    }

}
