<%-- 
    Document   : roomtype
    Created on : Jan 12, 2020, 11:41:20 AM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/WEB-INF/tlds/functions" prefix="f" %>


<!--================ PAGE-COVER =================-->
<div class="living_middle">
    <div class="container">
        <c:choose>
        <c:when test="${f:size(LIST_EVENTINSTANCE)>0}">
            <h2 class="title block-title">${LIST_EVENTINSTANCE[0].eventID.name}</h2>

            <div class="row">
                <c:forEach items="${LIST_EVENTINSTANCE}" var="item">
                    <div class="col-md-4 wow fadeInLeft animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">

                        <div class="living_box">

                            <img style="width: 330px;height: 220px"
                                 src="${item.urlAvatar}"
                                 class="img-responsive"
                                 alt="Thiếu file gốc">


                            <div class="living_desc">
                                <h3><a href="/eventinstance/${item.nameAscii}">${f:subString25(item.title)}</a></h3>
                                    <%--                    <p>${item.fieldIDs} </p>--%>
                            </div>
                            <a href="/eventinstance/${item.nameAscii}">
                                <table border="1" class="propertyDetails">
                                    <tbody>
                                    <tr>
                                        <td><img src="/resources/images/area.png" alt="" style="margin-right:7px;">Xem chi tiết</td>
                                    </tr>
                                    </tbody></table>
                            </a>
                        </div>

                    </div>
                </c:forEach>
            </div>
        </c:when>
            <c:otherwise><h5>Hiện không có sự kiện nào thuộc loại sự kiện này</h5></c:otherwise>
        </c:choose>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("title").text('Danh sách sự kiện ${LIST_EVENTINSTANCE[0].eventID.name} của .:DUHOOT:.')
    });
</script>