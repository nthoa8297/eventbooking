<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<input type="hidden" value="/Ebms/Admin/Content" id="reloadController"/>
<h1>Quản lý nhân viên</h1>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fab fa-servicestack"></i>
            Danh sách nhân viên
        </h3>

        <a  href="javascript:void(0);" data-controller="/Ebms/Admin/ViewInsert" class="btn btn-info float-right btn-open-modal">
            <i class="fas fa-plus"></i> Thêm nhân viên
        </a>
    </div>
    <div class="card-body" id="ajax-content">
        <c:import url="/views/admin/admin/admin_content.jsp"/> 
    </div>
    <div class="card-footer clearfix">
    </div>
</div>