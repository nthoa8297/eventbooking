<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri ="/WEB-INF/tlds/functions" prefix="f"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">    
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Ebms</title>
        <link href="/resources/shared/css/all.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link href="/resources/shared/css/icheck-bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/iqvmap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/adminlte.min.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/OverlayScrollbars.min.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/tempusdominus-bootstrap-4.css" rel="stylesheet" type="text/css"/>

        <link href="/resources/shared/noty/noty.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/noty/nest.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/summernote-bs4.min.css" rel="stylesheet" type="text/css"/>
        <script src="/resources/shared/js/jquery.min.js" type="text/javascript"></script>
        <link href="/resources/shared/datetime-picker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/fontawesome/fontawesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/data-table/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="/ckfinder/ckfinder.js" type="text/javascript"></script>
        <script src="/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="/ckeditor/config.js" type="text/javascript"></script>
        <link href="/resources/custom/customStyle.css" rel="stylesheet" type="text/css"/>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">

            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!--Left navbar links--> 
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>

                <!--Right navbar links--> 
                <ul class="navbar-nav ml-auto">
                    <!--Notifications Dropdown Menu--> 
                    <!--                                <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#">
                                                            <i class="far fa-bell"></i>
                                                            <span class="badge badge-warning navbar-badge">15</span>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <span class="dropdown-item dropdown-header">15 Notifications</span>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="#" class="dropdown-item">
                                                                <i class="fas fa-envelope mr-2"></i> 4 new messages
                                                                <span class="float-right text-muted text-sm">3 mins</span>
                                                            </a>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="#" class="dropdown-item">
                                                                <i class="fas fa-users mr-2"></i> 8 friend requests
                                                                <span class="float-right text-muted text-sm">12 hours</span>
                                                            </a>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="#" class="dropdown-item">
                                                                <i class="fas fa-file mr-2"></i> 3 new reports
                                                                <span class="float-right text-muted text-sm">2 days</span>
                                                            </a>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                                                        </div>
                                                    </li>-->
                    <li class="nav-item">
                        <a title="Đăng xuất" class="nav-link" href="/Ebms/Logout">
                            <i class="fas fa-sign-out-alt"></i>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <a href="javascript:void(0)" class="brand-link">
                    <!--<img src="/resources/images/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"/>-->
                    <span class="brand-text pull-right"><small>Chức vụ: ${ADMIN.adminRoleID.name}</small></span>
                </a>
                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="/resources/images/user.png" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block">${ADMIN.name}</a>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="nav-item has-treeview">
                                <c:if test="${ADMIN.adminRoleID.id==1}">
                                    <a href="/Ebms/Admin" class="nav-link">
                                        <i class="fas fa-user-tie"></i>
                                        <p>Quản lý nhân viên</p>
                                    </a>
                                </li>
                            <li class="nav-item has-treeview">
                                <a href="/Ebms/Provider" class="nav-link">
                                    <i class="fas fa-person-booth"></i>
                                    <p>Danh sách nhà cung cấp</p>
                                </a>
                            </li>
                                <li class="nav-item has-treeview">
                                    <a href="/Ebms/Address" class="nav-link">
                                        <i class="fas fa-person-booth"></i>
                                        <p>Danh sách địa chỉ</p>
                                    </a>
                                </li>
                                <li class="nav-item has-treeview">
                                    <a href="/Ebms/Event" class="nav-link">
                                        <i class="fab fa-typo3"></i>
                                        <p>Quản lý loại sự kiện</p>
                                    </a>
                                </li>
                            <li class="nav-item has-treeview">
                                <a href="/Ebms/EventInstance" class="nav-link">
                                    <i class="fab fa-typo3"></i>
                                    <p>Quản lý sự kiện</p>
                                </a>
                            </li>
                                <li class="nav-item has-treeview">
                                    <a href="/Ebms/Service" class="nav-link">
                                        <i class="fab fa-servicestack"></i>
                                        <p>Dịch vụ</p>
                                    </a>
                                </li>
                            </c:if>
                            <li class="nav-item has-treeview">
                                <a href="/Ebms/Customer" class="nav-link">
                                    <i class="fas fa-users"></i>
                                    <p>Danh sách khách hàng</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <!--                            <div class="col-sm-6">
                                                            <h1 class="m-0 text-dark">Dashboard</h1>
                                                        </div> /.col 
                                                        <div class="col-sm-6">
                                                            <ol class="breadcrumb float-sm-right">
                                                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                                                <li class="breadcrumb-item active">Dashboard v1</li>
                                                            </ol>
                                                        </div> /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">

                        <tiles:insertAttribute name="Content" />
                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->
            </div>

        </div>
        <!-- ./wrapper -->
        <script src="/resources/shared/js/jquery-ui.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/chart.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/jquery.vmap.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="/resources/shared/js/jquery.knob.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/summernote-bs4.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/jquery.overlayScrollbars.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/adminlte.min.js" type="text/javascript"></script>
        <!--<script src="/resources/shared/js/dashboard.js?v=23" type="text/javascript"></script>-->
        <script src="/resources/shared/js/demo.js" type="text/javascript"></script>
        <script src="/resources/custom/customJS.js" type="text/javascript"></script>
        <script src="/resources/shared/js/jquery.form.js" type="text/javascript"></script>
        <script src="/resources/shared/js/serialize.js" type="text/javascript"></script>
        <script src="/resources/shared/js/jquery.validate.js" type="text/javascript"></script>
        <script src="/resources/shared/noty/noty.min.js" type="text/javascript"></script>
        <script src="/resources/shared/datetime-picker/moment.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/tempusdominus-bootstrap-4.min.js" type="text/javascript"></script>
        <script src="/resources/shared/datetime-picker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/daterangpicker.js" type="text/javascript"></script>
        <script src="/resources/shared/fontawesome/fontawesome.min.js" type="text/javascript"></script>
        <script src="/resources/shared/data-table/jquery.dataTables.js" type="text/javascript"></script>
        <script src="/resources/shared/data-table/dataTables.bootstrap.js" type="text/javascript"></script>
        <script>
            jQuery.extend(jQuery.validator.messages, {
                required: "Thông tin này là bắt buộc!",
                remote: "Thông tin nhập vào không chính xác",
                equalTo: "Dữ liệu không trùng khớp",
                maxlength: jQuery.validator.format("Nhập tối đa {0} ký tự"),
                minlength: jQuery.validator.format("Nhập tối thiểu {0} ký tự"),
                max: jQuery.validator.format("Nhập giá trị tối đa lớn hơn hoặc bằng{0}"),
                min: jQuery.validator.format("Nhập giá trị tối thiểu lớn hơn hoặc bằng {0}")
            });
        </script>
        <script>
            $(document).ready(function () {
                $('a[title]').tooltip();
                $('.data-table').DataTable({
//                    "paging": true,
//                    "lengthChange": false,
//                    "searching": false,
//                    "ordering": true,
//                    "info": true,
//                    "autoWidth": false,
                });
                $.each($('.nav-link'), function (i, v) {
                    if ($(v).attr('href') === '${URL_ACTIVE}') {
                        $(v).addClass('active');
                    }
                });
            });
        </script>
    </body>

</html>