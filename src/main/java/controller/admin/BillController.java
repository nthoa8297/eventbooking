/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import entity.Bill;
import entity.Ticket;
import handle.BillHandle;
import handle.CustomerHandle;
import handle.TicketHandle;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import util.Alert;

/**
 *
 * @author Admin
 */
@Controller
@RequestMapping("/Ebms/Bill")
public class BillController {

    @RequestMapping
    public String bill(
            @RequestParam(name = "currentPage", required = false, defaultValue = "1") Integer currentPage,
            ModelMap mm) throws Exception {
        Map pager = new HashMap();
        pager.put("currentPage", currentPage);
        List<Bill> listItem = new BillHandle().paging(pager);
        mm.put("LIST_ITEM", listItem);
        mm.put("PAGER", pager);
        return "Admin.Bill";
    }

    @RequestMapping(value = "/Content", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView billContent(
            @RequestParam(name = "currentPage", required = false, defaultValue = "1") Integer currentPage,
            ModelMap mm) throws Exception {
        Map pager = new HashMap();
        pager.put("currentPage", currentPage);
        List<Bill> listItem = new BillHandle().paging(pager);
        mm.put("LIST_ITEM", listItem);
        mm.put("PAGER", pager);
        return new ModelAndView("Admin.Bill.Content");
    }

    @RequestMapping(value = "/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView viewInsert(ModelMap mm) throws Exception {
        mm.put("LIST_CUSTOMER", new CustomerHandle().findAllAvailable());
        return new ModelAndView("Admin.Event.ViewInsert");
    }

//    @RequestMapping(value = "/FindAllTicketAvaiable", method = RequestMethod.GET)
//    @ResponseBody
//    public List<Ticket> findAllTicketAvaiable(
//            @RequestParam(name = "eventInstanceID", required = true) Integer eventInstanceID,
//            @RequestParam(name = "isGuests", required = true) boolean isGuests
//    ) throws Exception {
//        return new TicketHandle().findAllTicketAvaiable(eventInstanceID, isGuests);
//    }

    @RequestMapping(value = "/Insert", method = RequestMethod.POST)
    @ResponseBody
    public Pair insert(@RequestBody Bill bill) {
        try {
            new BillHandle().save(bill);
            return new Pair(0, Alert.createErrorAlert("Tạo hoá đơn thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Tạo hoá đơn thất bại"));
        }
    }
}
