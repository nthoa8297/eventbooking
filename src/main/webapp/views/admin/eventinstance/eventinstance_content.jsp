<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<c:choose>
    <c:when test="${f:size(LIST_ITEM)==0}">
        <span class="alert alert-danger">Không có bản ghi nào tồn tại!</span>
    </c:when>
    <c:otherwise>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 1%">STT</th>
                        <th>Sự kiện</th>
                        <th>Ảnh minh hoạ</th>
                        <th>Địa điểm tổ chức</th>
                        <th>Lĩnh vực quan tâm</th>
                        <th>Ngày bắt đầu</th>
                        <th>Ngày kết thúc</th>
                        <th>Chi phí dự kiến</th>
                        <th>Trạng thái</th>
                        <th style="width: 40px"></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${LIST_ITEM}" var="item" varStatus="index">
                        <tr>
                            <td>${(index.index+1)+(PAGER.currentPage-1!=0?(PAGER.currentPage-1)*10:0)}</td>
                            <td><a href="/eventinstance/${item.nameAscii}">${item.title} (${item.eventID.name})</a></td>
                            <td><img style="height:100px;width: 150px" src="${item.urlAvatar}"></td>
                            <td>${item.addressID.name }</td>
                            <td>${item.eventID.fieldIDs}</td>
                            <td>${f:customFormatDate("dd/MM/yyyy", item.startDate)}</td>
                            <td>${f:customFormatDate("dd/MM/yyyy", item.endDate)}</td>
                            <td>${f:quantityEventInstance(item.serviceMappings)}</td>
                            <td>${f:getTrangThai(item.status)}</td>
                            <td style="white-space: nowrap;">
                                <c:if test="${item.status ==0}">
                                    <a title="Điều chỉnh" class="btn-open-modal"  href="javascript:void(0)"
                                       data-controller="/Ebms/EventInstance/ViewEdit/${item.id}"><i class="fas fa-edit text-primary"></i></a>
                                    <a title="Bắt đầu bán vé" class="btn-send-ajax" href="javascript:void(0)"
                                       data-controller="/Ebms/EventInstance/ChangeStatus/${item.id}?status=1"><i class="fas fa-ticket-alt"></i></a>
                                    <a title="Huỷ tiến hành" class="btn-send-ajax" href="javascript:void(0)"
                                       data-controller="/Ebms/EventInstance/ChangeStatus/${item.id}?status=4"><i class="fas fa-trash-alt text-danger"></i></a>
                                    </c:if>
                                    <c:if test="${item.status ==1}">
                                    <a title="Bán vé" class="btn-open-modal sell" href="javascript:void(0)"
                                       data-controller="/Ebms/EventInstance/SellTicket/${item.id}">
                                        <c:choose>
                                            <c:when test="${item.vipTicket>=10}">
                                                <small class="badge badge-success"><i class="fas fa-hand-holding-usd"></i>${item.vipTicket}VIP</small>
                                            </c:when>
                                            <c:when test="${item.vipTicket>0&&item.vipTicket<10}">
                                                <small class="badge badge-warning">${item.vipTicket}VIP</small>
                                            </c:when>
                                            <c:when test="${item.vipTicket==0}">
                                                <small class="badge badge-danger">${item.vipTicket}VIP</small>
                                            </c:when>
                                        </c:choose>
                                        <c:choose>
                                            <c:when test="${item.normalTicket>=10}">
                                                <small class="badge badge-success"><i class="fas fa-hand-holding-usd"></i>${item.normalTicket}NOR</small>
                                            </c:when>
                                            <c:when test="${item.normalTicket>0&&item.normalTicket<10}">
                                                <small class="badge badge-warning">${item.normalTicket}NOR</small>
                                            </c:when>
                                            <c:when test="${item.normalTicket==0}">
                                                <small class="badge badge-danger">${item.normalTicket}NOR</small>
                                            </c:when>
                                        </c:choose>
                                    </a>
                                    <a title="Bắt đầu sự kiện" class="btn-send-ajax" href="javascript:void(0)"
                                       data-controller="/Ebms/EventInstance/ChangeStatus/${item.id}?status=2"><i class="fas fa-hourglass-end text-success"></i></a>
                                    </c:if>
                                    <c:if test="${item.status ==2}">
                                    <a title="Kết thúc sự kiện" class="btn-send-ajax" href="javascript:void(0)"
                                       data-controller="/Ebms/EventInstance/ChangeStatus/${item.id}?status=3"><i class="fas fa-hourglass-end text-success"></i></a>
                                    </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <c:if test="${PAGER.totalPage>1}">
                <div class="card-tools">
                    <ul class="pagination pagination-sm float-right">
                        <li class="page-item"><a class="page-link" href="/Ebms/EventInstance?currentPage=1${PAGER.status!=null?'&status=':''}${PAGER.status}">«</a></li>
                        <c:forEach begin="1" end="${PAGER.totalPage}" var="item">
                            <c:if test="${PAGER.currentPage-item<=3||(PAGER.currentPage-item>=-3)}">
                                <li class="page-item"><a class="page-link" href="/Ebms/EventInstance?currentPage=${item}${PAGER.status!=null?'&status=':''}${PAGER.status}">${item}</a></li>
                            </c:if>
                        </c:forEach>
                        <li class="page-item"><a class="page-link" href="/Ebms/EventInstance?currentPage=${PAGER.totalPage}${PAGER.status!=null?'&status=':''}${PAGER.status}">»</a></li>
                    </ul>
                </div>
            </c:if>
        </div>
    </c:otherwise>
</c:choose>

