<%-- 
    Document   : roomtype
    Created on : Jan 12, 2020, 11:41:20 AM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri ="/WEB-INF/tlds/functions" prefix="f"%>

<!--============= PAGE-COVER =============-->
<section class="page-cover" id="cover-login">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Login Page</h1>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Login Page</li>
                </ul>
            </div><!-- end columns -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end page-cover -->


<!--===== INNERPAGE-WRAPPER ====-->
<section class="innerpage-wrapper">
    <div id="login" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="flex-content">
                        <div class="custom-form custom-form-fields">
                            <h3>Đăng nhập</h3>
                            <p>Rất nhiều điều thú vị đang chờ bạn khám phá!</p>
                            <form id="form-login" novalidate action="/login">

                                <div class="form-group">
                                    <input type="text" name="username" class="form-control" placeholder="Tài khoản"  required/>
                                </div>

                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Mật khẩu"  required/>
                                </div>

                                <button class="btn btn-orange btn-block">Đăng nhập</button>
                            </form>

                            <div class="other-links">
                                <p class="link-line">Bạn chưa có tài khoản ? <a href="/register">Đăng ký ngay!</a></p>
                            </div><!-- end other-links -->
                        </div><!-- end custom-form -->

                        <div class="flex-content-img custom-form-img">
                            <img src="/resources/images/home/login.jpg"  class="img-responsive" alt="registration-img"/>
                        </div><!-- end custom-form-img -->
                    </div><!-- end form-content -->

                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->         
    </div><!-- end login -->
</section><!-- end innerpage-wrapper -->
<script>
    $(document).ready(function () {
        $('#form-login').validate({
            submitHandler: function () {
                var data = $('#form-login').serializeObject();
                var url = $('#form-login').attr('action');
                var url_string = window.location.href;
                var url = new URL(url_string);
                var r = url.searchParams.get("r");
                data.r = r;
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    success: function (result) {
                        openAlert(result.value);
                    },
                    error: function () {
                        errorAlert();
                    }
                });
            }
        });
    });
</script>