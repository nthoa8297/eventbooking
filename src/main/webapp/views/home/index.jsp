<%-- 
    Document   : services
    Created on : Jan 12, 2020, 10:53:40 AM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/WEB-INF/tlds/functions" prefix="f" %>
<!--=============== HOTEL OFFERS img size 360x240===============-->
<div class="container" style="height: 500px">
<div class="content_middle_box">
    <div class="clearfix"></div>
    <h2 class="title block-title">Top 5 sự kiện nổi bật</h2>
    <div class="top_grid">
        <c:if test="${f:size(AVAILABLE_EVENTINSTANCE)>=3}">
        <c:forEach items="${AVAILABLE_EVENTINSTANCE}" var="item" begin="0" end="2">
        <div class="col-md-4">
            <div class="grid1">
                <div class="view view-first">
                    <div class="index_img"><img src="${item.urlAvatar}" style="height: 173px;width: 350px"
                                                class="img-responsive" alt="" data-insta_upload_ext_elem="1"></div>
                     <a href="/eventinstance/${item.nameAscii}">
                        <div class="mask">
                            <div class="info"><i class="search"> </i> Xem chi tiết</div>
                         </div>
                    </a>
                </a>
            </div>
            <div class="inner_wrap">
                <h3>${item.title}</h3>
            </div>
        </div>
    </div>
    </c:forEach>
    </c:if>
    <div class="clearfix"></div>
    <c:if test="${f:size(AVAILABLE_EVENTINSTANCE)==5}">
        <c:forEach items="${AVAILABLE_EVENTINSTANCE}" var="item" begin="3" end="4">
            <div class="col-md-6">
                <div class="grid1">
                    <div class="view view-first">
                        <div class="index_img">
                            <img src="${item.urlAvatar}" style="width: 540px;height: 224px;"
                                                    class="img-responsive" alt="" data-insta_upload_ext_elem="1"></div>
                        <a href="/eventinstance/${item.nameAscii}">
                        <div class="mask" style="width: 540px!important;height: 224px;!important;">
                            <div class="info"><i class="search"> </i>Xem chi tiết</div>
                        </div>
                        </a>
                    </div>
                    <div class="inner_wrap">
                        <h3>${item.title}</h3>
                    </div>
                </div>
            </div>
        </c:forEach>
    </c:if>
</div>
</div>
</div>
<div class="living_bottom">
    <br>
    <div class="container">      <br>
        <h2 class="title block-title">Loại sự kiện nổi bật</h2>
        <div class="row">
            <c:forEach items="${LIST_EVENT}" var="item">
                <div class="col-md-4 wow fadeInLeft animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">

                    <div class="living_box">

                        <img style="width: 330px;height: 220px"
                             src="${item.urlAvatar}"
                             class="img-responsive"
                             alt="Thiếu file gốc">


                        <div class="living_desc">
                            <h3><a href="/event/${item.nameAscii}">${item.name}</a></h3>
                                <%--                    <p>${item.fieldIDs} </p>--%>
                        </div>
                        <a href="/event/${item.nameAscii}">
                            <table border="1" class="propertyDetails">
                                <tbody>
                                <tr>
                                    <td><img src="/resources/images/area.png" alt="" style="margin-right:7px;">Xem chi tiết</td>
                                </tr>
                                </tbody></table>
                        </a>
                    </div>

                </div>
            </c:forEach>
        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        $("title").text('Trang chủ .:DUHOOT:.')
    });
</script>