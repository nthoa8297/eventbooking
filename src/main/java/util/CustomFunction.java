package util;

import entity.ServiceMapping;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomFunction {

    public static boolean isEmpty(String input) {

        return input == null || input.trim().length() == 0;
    }

    public static String stringOf(Integer i) {
        return i.toString();
    }

    public static int size(Collection c) {
        return c == null ? 0 : c.size();
    }

    public static String customFormatDate(String format, Date date) {
        return date == null ? null : new SimpleDateFormat(format).format(date);
    }

    public static String customFormatDecimal(String format, BigDecimal num) {
        num = num == null ? BigDecimal.ZERO : num;
        return new DecimalFormat(format).format(num.setScale(0, RoundingMode.HALF_UP));
    }

    public static double roundDouble(Double input, Integer scale) {
        double scaleValue = Math.pow(10, Double.parseDouble(scale.toString()));
        input = input * scaleValue;
        String s = input.toString();
        return Math.round(Float.parseFloat(s)) / scaleValue;
    }
    public static  String subString25(String input){
        if(input.length()>30){
            input=input.substring(0,30);
        }
        return input+"...";
    }
    public static String getTrangThai(Integer i) {
        String str = "";
        switch (i) {
            case 0:
                str = "Đang tổ chức";
                break;
            case 1:
                str = "Đang bán vé";
                break;
            case 2:
                str = "Đang diễn ra";
                break;
            case 3:
                str = "Đã kết thúc";
                break;
            case 4:
                str = "Đã bị huỷ";
                break;
            default:
                str = "Trạng thái không hợp lệ̣!";
        }
        return str;
    }

    public static Boolean isStringContainInteger(String s, Integer i) {
        if(s.length()>0){
        for (String sub : s.split(",")) {
            try {
                if (Integer.parseInt(sub) == i) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        }
        return false;
    }

    public static String md5(String input) {
        MessageDigest md;
        StringBuffer sB;
        String output = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            byte[] digest = md.digest();
            sB = new StringBuffer();
            for (byte b : digest) {
                sB.append(String.format("%02x", b & 0xff));
            }
            output = sB.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CustomFunction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }

    public static Integer dateDiff(Date startDate, Date endDate) {
        long diff = startDate.getTime() - endDate.getTime();
        return (int) (diff / (24 * 60 * 60 * 1000)) + 2;
    }

    public static String countDownTime(Date date,Integer status) {
        long diff = 0;
        Date currentDate = new Date();
        if (status != null && date != null
                && status.compareTo(0) == 0
                && currentDate.compareTo(date) ==-1) {
            long diffInMillies = Math.abs(currentDate.getTime() - date.getTime());
            diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            diff++;
        }
        return String.valueOf(diff);
    }
    public static String quantityEventInstance(List<ServiceMapping> lst) {
        Integer quantity=0;
        if(lst.size()>0){
            for (ServiceMapping dto : lst){
                quantity+= dto.getQuantity()*dto.getServiceID().getPrice().intValue();
            }
        }
        return customFormatDecimal("###,### đ",new BigDecimal(quantity));
    }
}
