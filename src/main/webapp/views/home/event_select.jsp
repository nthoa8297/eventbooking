<%-- 
    Document   : roomtype
    Created on : Jan 12, 2020, 11:41:20 AM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/WEB-INF/tlds/functions" prefix="f" %>


<!--================ PAGE-COVER =================-->

<div class="living_middle">

    <div class="container" style="background-color: white">
        <div class="col-md-12 wow fadeInRight animated" data-wow-delay="0.4s">
            <div class="educate_grid">
                <div class="col-md-12">
                    <div class="living_box">
                        <div class="img-responsive">

                            ${SELECT_EVENT.seoContent}


                                <div class="living_desc desc1">
                                    <h3>
                                        <a href="/events/${SELECT_EVENT.nameAscii}">Sự kiện đã tổ chức</a>
                                    </h3>
                                    <p></p>
                                    <p class="educate"></p>
                                    <p class="price pr_box"></p>
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("title").text('Danh sách sự kiện  của .:DUHOOT:.')
    });
</script>
