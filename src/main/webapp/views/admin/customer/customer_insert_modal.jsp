<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div id="myModal" class="modal fade">
    <style type="text/css">
        select, ul { height: 100px; overflow: auto; width: 100px; }
        ul { list-style-type: none; margin: 0; padding: 0; overflow-x: hidden;width: auto; }
        li { margin: 0; padding: 0; }
        label { display: block; color: WindowText; background-color: Window; margin: 0; padding: 0; width: 100%; }
        label:hover { background-color: Highlight; color: HighlightText; }
    </style>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">          
            <form id="my-form" novalidate action="/Ebms/Customer/Insert">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm khách hàng</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Tên khách hàng</label>
                            <input name="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Tài khoản</label>
                            <input name="username" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Mật khẩu</label>
                            <input name="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Lĩnh vực quan tâm</label>
                            <ul>
                                <c:forEach items="${LIST_FIELD}" var="item">
                                    <li class="custom-control custom-checkbox">
                                        <label for="field${item.id}" class="form-check-label">
                                            <input type="checkbox" value="${item.id}" name="fieldCaringIDs" id="field${item.id}" class="custom-control-input">
                                            <label class="custom-control-label" for="field${item.id}">${item.name}</label>
                                        </label>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="form-group">
                            <label>Sự kiện quan tâm</label>
                            <ul>
                                <c:forEach items="${LIST_EVENT}" var="item">
                                    <li class="custom-control custom-checkbox">
                                        <label for="check${item.id}" class="form-check-label">
                                            <input type="hidden" id="fieldIDs${item.id}" value="${item.fieldIDs}">
                                            <input type="checkbox" value="${item.id}" name="eventCaringIDs"
                                                   id="check${item.id}" class="custom-control-input">
                                            <label class="custom-control-label" for="check${item.id}">${item.name}</label>
                                        </label>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="form-group">
                            <label>Ngày sinh</label>
                            <input id="dateOfBirth" class="form-control datetimepicker" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Huỷ</button>
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <script>
        $("input[name='eventCaringIDs']").on('click',function () {
            $.each($("input[name='eventCaringIDs']:checked"), function () {
                var input = $(this);
                var idEvent = input.attr("id").split("check")[1];
                var idField = document.getElementById("fieldIDs"+idEvent);
                var lstFieldIDs = idField.value.split(',');
                for(var i=0;i<lstFieldIDs.length;i++){
                    var fieldInput = document.getElementById("field"+lstFieldIDs[i]);
                    fieldInput.checked =true;
                }
            });
        });
        $(document).ready(function () {
            $('#my-form').validate({
                submitHandler: function () {
                    var data = $('#my-form').serializeObject();
                    if ($('#dateOfBirth').val()) {
                        data.dateOfBirth = new Date(moment($('#dateOfBirth').val(), 'DD/MM/YYYY').format('YYYY/MM/DD'));
                    }
                    var fields = [];
                    $.each($("input[name='fieldCaringIDs']:checked"), function () {
                        fields.push($(this).val());
                    });
                    var events = [];
                    $.each($("input[name='eventCaringIDs']:checked"), function () {
                        events.push($(this).val());
                    });
                    data.fieldCaringIDs = fields.join(",");
                    data.eventCaringIDs = events.join(",");
                    var url = $('#my-form').attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function (result) {
                            openAlert(result.value);
                            if (result.key) {
                                $('#myModal').modal('hide');
                                reloadAjaxContent();
                            }
                        },
                        error: function () {
                            errorAlert();
                        }
                    });
                }
            });
            var firstOpen;
            $('.datetimepicker').datetimepicker({
                viewMode: 'years',
                format: 'DD/MM/YYYY',
                icons: {
                    previous: 'fas fa-angle-left',
                    next: 'fas fa-angle-right',
                }
            }).on("dp.show", function () {
                if (typeof firstOpen === 'undefined') {
                    $(this).data('DateTimePicker').date("01/01/1997");
                    firstOpen = false;
                }
            });
        });
    </script>
</div>