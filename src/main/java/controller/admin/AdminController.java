/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import controller.*;
import entity.Admin;
import handle.AdminHandle;
import handle.AdminRoleHandle;
import javafx.util.Pair;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import util.Alert;
import util.CustomFunction;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
@Controller
@RequestMapping("/Ebms/Admin")
public class AdminController {

    @RequestMapping
    public ModelAndView Admin(ModelMap mm) throws Exception {
        mm.put("LIST_ITEM", new AdminHandle().findAllAvailable());
        return new ModelAndView("Admin.Admin");
    }

    @RequestMapping(value = "/Content", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView adminContent(ModelMap mm) throws Exception {
        mm.put("LIST_ITEM", new AdminHandle().findAllAvailable());
        return new ModelAndView("Admin.Admin.Content");
    }

    @RequestMapping(value = "/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView viewInsert(ModelMap mm) throws Exception {
        mm.put("AVAILABLE_ADMINROLES", new AdminRoleHandle().findAll());
        return new ModelAndView("Admin.Admin.ViewInsert");
    }

    @RequestMapping(value = "/Insert", method = RequestMethod.POST)
    @ResponseBody
    public Pair insert(@RequestBody Admin admin, HttpSession session) {
        Admin logedAdmin = (Admin) session.getAttribute("ADMIN");
        try {
            admin.setPassword(CustomFunction.md5(admin.getPassword()));
            new AdminHandle().create(admin);
            return new Pair(1, Alert.createSuccessAlert("Thêm nhân viên thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Thêm nhân viên thất bại"));
        }
    }

    @RequestMapping(value = "/ViewEdit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewEdit(@PathVariable(value = "id") Integer id, ModelMap mm) throws Exception {
        mm.put("AVAILABLE_ADMINROLES", new AdminRoleHandle().findAll());
        mm.put("SELECTED_ITEM", new AdminHandle().findEager(id));
        return new ModelAndView("Admin.Admin.ViewEdit");
    }

    @RequestMapping(value = "/Edit", method = RequestMethod.POST)
    @ResponseBody
    public Pair Edit(@RequestBody Admin admin, HttpSession session) {
        Admin logedAdmin = (Admin) session.getAttribute("ADMIN");
        try {
            new AdminHandle().update(admin);
            return new Pair(1, Alert.createSuccessAlert("Chỉnh sửa nhân viên thành công"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Chỉnh sửa nhân viên thất bại"));
        }
    }

    @RequestMapping(value = "/Delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Pair deleteAdminRole(@PathVariable(value = "id") Integer id) {
        try {
            Admin admin = new AdminHandle().findEager(id);
            if (admin.getAdminRoleID().getId() == 1) {
                return new Pair(0, Alert.createErrorAlert("Không thể xoá tài khoản " + admin.getAdminRoleID().getName()));
            }
            new AdminHandle().delete(id);
            return new Pair(1, Alert.createSuccessAlert("Xoá nhân viên thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Xoá nhân viên thất bại"));
        }
    }
}
