<%-- 
    Document   : header
    Created on : Jan 12, 2020, 9:42:37 AM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri ="/WEB-INF/tlds/functions" prefix="f"%>
<div class="header">
    <div class="col-sm-8 header-left">
        <div class="logo">
            <%--                style="background-image: url(/resources/images/home/favicon.png);height:52px ;width: 52px"--%>
            <a href="/">
                <img src="/resources/images/logo.png" alt="" />
            </a>
        </div>
        <div class="menu">
            <a class="toggleMenu" href="#"><img src="/resources/images/nav.png" alt="" /></a>
            <ul class="nav" id="nav">
                <li class="active"><a href="/">Trang chủ</a></li>
                <li><a href="/eventsinstance">Sự kiện</a></li>
                <c:if test="${CUSTOMER_CURRENT != null}">
                    <li><a href="/eventsinstanceCare">Lĩnh vực quan tâm</a></li>
                </c:if>
                <li><a href="/introduce">Giới thiệu</a></li>
                <li><a href="/contact">Liên hệ</a></li>
                <div class="clearfix"></div>
            </ul>
            <script type="text/javascript" src="/resources/shared/js/responsive-nav.js"></script>
        </div>
        <!-- start search-->
        <div class="search-box">
            <div id="sb-search" class="sb-search">
                <form>
                    <input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
                    <input class="sb-search-submit" type="submit" value="">
                    <span class="sb-icon-search"> </span>
                </form>
            </div>
        </div>
        <!----search-scripts---->
        <script src="/resources/shared/js/classie.js"></script>
        <script src="/resources/shared/js/uisearch.js"></script>
        <script>
            new UISearch( document.getElementById( 'sb-search' ) );
        </script>
        <!----//search-scripts---->
        <div class="clearfix"></div>
    </div>
    <div class="col-sm-4 header_right">
        <c:choose>
            <c:when test="${f:isEmptyString(CUSTOMER_CURRENT.username)}">
                <div id="loginContainer">
                    <a href="#" id="loginButton"><img src="/resources/images/login.png"><span>Đăng nhập</span></a>
                    <span>||</span>
                    <a href="register"><span>Đăng ký</span></a>
                    <div id="loginBox">
                        <form id="loginForm" novalidate action="/login">
                            <fieldset id="body">
                                <fieldset>
                                    <label for="username">Tên tài khoản</label>
                                    <input type="text" class="form-control" id="username" name="username" value="nthoa"
                                           placeholder="Tài khoản" required autofocus>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-lock"></span>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <label for="password">Mật khẩu</label>
                                    <input type="password" class="form-control" id="password" name="password"
                                           value="123" placeholder="Mật khẩu" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-lock"></span>
                                        </div>
                                    </div>
                                </fieldset>
                                <button type="submit" id="login" >Xác Nhận</button>
                            </fieldset>
                        </form>
                    </div>
                </div>

            </c:when>
            <c:otherwise>
            <div id="loginContainer" class="infoButton"><img src="/resources/images/login.png"><span>Xin chào! <a href="/info/thongtintaikhoan" style="color: white">${CUSTOMER_CURRENT.name}</a></span>
            </c:otherwise>
        </c:choose>


        <div class="clearfix"></div>
    </div>

    <div class="clearfix"></div>
</div>
</div>

<script>
    $('#loginForm').validate({
        rules: {
            username: {
                required: true
            },
            password: {
                required: true
            }
        },
        messages: {
            username: {
                required: 'Hãy nhập tài khoản'
            },
            password: {
                required: 'Hãy nhập mật khẩu'
            }
        },
        submitHandler: function () {

            var data = $('#loginForm').serializeObject();
            var url_string = window.location.href;
            var url = new URL(url_string);
            // var r = url.searchParams.get("r");
            data.r = url;
            $.ajax({
                url: '/login',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function (result) {
                    openAlert(result.value);
                },
                error: function () {
                    errorAlert();
                }
            });
        }
    });
</script>

