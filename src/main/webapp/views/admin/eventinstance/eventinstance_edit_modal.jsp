<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div id="myModal" class="modal fade" >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form id="my-form"  action="/Ebms/EventInstance/Edit">
                <input name="id" type="hidden" value="${SELECTED_ITEM.id}"/>
                <div class="modal-header">
                    <h4 class="modal-title">Điều chỉnh sự kiện</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Tiêu đề</label>
                            <input value="${SELECTED_ITEM.title}" name="title" onkeyup="$('#nameAscii[readonly]').val(removeUnicodeURL($(this).val()))" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Đường dẫn</label>
                            <input value="${SELECTED_ITEM.nameAscii}" id="nameAscii" name="nameAscii" class="form-control" required readonly>
                        </div>
                        <div class="form-group">
                            <label>Địa chỉ</label>
                            <select required class="form-control" name="addressID" data-type="int" data-json="id">
                                <option selected>-Lựa chọn địa chỉ-</option>
                                <c:forEach items="${LIST_ADDRESS}" var="item">
                                    <option ${SELECTED_ITEM.addressID.id==item.id?'selected':''} value="${item.id}">${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Loại sự kiện</label>
                            <select required class="form-control" name="eventID" data-type="int" data-json="id">
                                <option selected>-Lựa chọn loại sự kiện-</option>
                                <c:forEach items="${LIST_EVENT}" var="item">
                                    <option ${SELECTED_ITEM.eventID.id==item.id?'selected':''} value="${item.id}">${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Ảnh minh hoạ</label>
                            <div class="input-group">
                                <input value="${SELECTED_ITEM.urlAvatar}" type="text"  class="form-control" name="urlAvatar" id="urlAvatar"/>
                                <div class="input-group-append">
                                    <button type="button" class="input-group-text" id="btn-finder">Chọn file</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Ngày bắt đầu</label>
                            <input  value="${f:customFormatDate('dd-MM-yyyy',SELECTED_ITEM.startDate)}" id="startDate" class="form-control datetimepicker" required>
                        </div>
                        <div class="form-group">
                            <label>Ngày kết thúc</label>
                            <input  value="${f:customFormatDate('dd-MM-yyyy',SELECTED_ITEM.endDate)}" id="endDate" class="form-control datetimepicker" required>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Số vé khách mời</label>
                                    <input type="number" name="vipTicket" class="form-control" min="1" value="${SELECTED_ITEM.vipTicket}" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Số vé khách thường</label>
                                    <input type="number" name="normalTicket" class="form-control" min="1" value="${SELECTED_ITEM.normalTicket}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Giá vé</label>
                            <input name="ticketPrice" class="form-control" value="${SELECTED_ITEM.ticketPrice}" required>
                        </div>
                        <div>
                            <fieldset>
                                <legend style="text-align: center;">Thông tin dịch vụ</legend>
                                <table  class="table">
                                    <thead class="thead-light">
                                    <th></th>
                                    <th>
                                        <select class="form-control" id="serviceID">
                                            <option value="" data-price="0" data-priceformated="0 đ">--Lựa chọn dịch vụ--</option>
                                            <c:forEach items="${LIST_SERVICE}" var="item">
                                                <option data-price="${item.price}"
                                                        data-priceformated="${f:customFormatDecimal('###,###,### đ',item.price)}"
                                                        value="${item.id}">${item.name}</option>
                                            </c:forEach>
                                        </select>
                                    </th>
                                    <th><input class="form-control" readonly type="text" id="price" value="0 đ"/></th>
                                    <th><input class="form-control" type="number" id="quantity" value="1" min="1"/></th>
                                    <th><input class="form-control" readonly type="number" id="amount" value="0" min="0"/></th>
                                    <th><a class="form-control" id="btn-save-detail" href="javascript:void(0)"><i class="fas fa-save text-success"></i></a></th>
                                    </thead>
                                    <thead class="thead-light">
                                    <th>STT</th>
                                    <th>Dịch vụ</th>
                                    <th>Đơn giá</th>
                                    <th>Số lượng</th>
                                    <th>Thành tiền</th>
                                    <th></th>
                                    </thead>
                                    <tbody id="details">
                                        <c:set var="index" value="1"/>
                                        <c:forEach items="${SELECTED_ITEM.serviceMappings}" var="item">
                                            <tr  class="detail" id="detail-${index}" data-detail='{"serviceID":{"id":${item.serviceID.id}},"quantity":${item.quantity}}'>
                                                <td>${index}</td>
                                                <td>${item.serviceID.name}</td>
                                                <td>${f:customFormatDecimal('###,###,### <sup>đ</sup>',item.serviceID.price)}</td>
                                                <td>${item.quantity}</td>
                                                <td>${f:customFormatDecimal('#',item.serviceID.price*item.quantity)}</td>
                                                <td class="text-center" style="white-space: nowrap;">
                                                    <span class="detail-action action-edit text-info fas fa-edit" style="margin-right: 3px;"></span>
                                                    <span class="detail-action action-delete text-danger fas fa-trash-alt"></span>
                                                </td>
                                            </tr>
                                            <c:set var="index" value="${index+1}"/>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </fieldset>
                        </div>
                        <div class="form-group">
                            <label>Nội dung trang</label>
                            <textarea class="form-control ckeditor-content" name="description" id="ckeditor" rows="7">${SELECTED_ITEM.description}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Huỷ</button>
                    <button type="button" id="btn-submit" class="btn btn-primary">Lưu</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('ckeditor', {
                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?Type=Images',
                filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?Type=Flash',
                filebrowserUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images',
                language: 'vi',
                height: 300
            });

            CKFinder.config.startupPath = "/images/";
            $(document).on("click", "#btn-finder", function () {
                var finder = new CKFinder({
                    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                    filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?Type=Images',
                    filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?Type=Flash',
                    filebrowserUploadUrl: '/ckfinder/core/connector/java/connector.aspx?command=QuickUpload&type=Files',
                    filebrowserImageUploadUrl: '/ckfinder/core/connector/java/connector.aspx?command=QuickUpload&type=Images',
                    filebrowserFlashUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images'
                });
                finder.selectActionFunction = function (fileUrl) {
                    $('#urlAvatar').val(fileUrl);
                };
                finder.popup();
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#btn-submit').unbind('click');
            $('#btn-submit').click(function () {
                $('#my-form').submit();
            });
            $('#my-form').validate({
                submitHandler: function () {
                    var data = $('#my-form').serializeObject();
                    if ($('#startDate').val()) {
                        data.startDate = new Date(moment($('#startDate').val(), 'DD/MM/YYYY').format('YYYY/MM/DD'));
                    }
                    if ($('#endDate').val()) {
                        data.endDate = new Date(moment($('#endDate').val(), 'DD/MM/YYYY').format('YYYY/MM/DD'));
                    }
                    data.serviceMappings = getDetails();
                    data.description = CKEDITOR.instances.ckeditor.getData();
                    var url = $('#my-form').attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function (result) {
                            openAlert(result.value);
                            if (result.key) {
                                $('#myModal').modal('hide');
                                reloadAjaxContent();
                            }
                        },
                        error: function () {
                            errorAlert();
                        }
                    });
                }
            });
            $('.datetimepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                icons: {
                    previous: 'fas fa-angle-left',
                    next: 'fas fa-angle-right',
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
            });
        });
    </script>
    <script>
        var editRow;
        $('#btn-save-detail').on('click', function () {
            var rowIndex = parseInt($('#details>tr:last-child>td').html());
            if (isNaN(rowIndex)) {
                rowIndex = 0;
            }

            if (!$('#serviceID').val()) {
                $('#serviceID').focus();
                return;
            } else if (isNaN($('#quantity').val()) || $('#quantity').val() < 0 || $('#quantity').val() == '') {
                $('#quantity').focus();
                return;
            }


            var details = document.getElementById('details');

            var row;
            var cellSTT;
            var cellProduct;
            var cellPrice;
            var cellQuantity;
            var cellAmount;
            var cellAction;
            var product = $('#serviceID option:selected').html();
            var serviceID = $('#serviceID option:selected').val();
            var price = $('#serviceID option:selected').data('price');
            var priceformated = $('#serviceID option:selected').data('priceformated');
            var quantity = $('#quantity').val();
            var amount = $('#amount').val();

            if (editRow) {
                row = editRow.cloneNode(true);
                $(editRow).replaceWith(row);
                $(editRow).remove();
                var cells = row.getElementsByTagName('td');
                cellSTT = cells[0];
                cellProduct = cells[1];
                cellPrice = cells[2];
                cellQuantity = cells[3];
                cellAmount = cells[4];
                editRow = null;
            } else {
                row = details.insertRow(rowIndex);
                row.className = 'detail';
                var rowID = 'detail-' + (rowIndex + 1);
                row.id = rowID;
                cellSTT = row.insertCell(0);
                cellSTT.classList.add('text-center');
                cellSTT.innerHTML = rowIndex + 1;
                //
                cellProduct = row.insertCell(1);
                cellPrice = row.insertCell(2);
                cellQuantity = row.insertCell(3);
                cellAmount = row.insertCell(4);
                cellAction = row.insertCell(5);
                cellAction.classList.add('text-center');
                cellAction.style.whiteSpace = 'nowrap';

                var buttonEdit = document.createElement("span");
                buttonEdit.className += 'detail-action action-edit text-info fas fa-edit';
                buttonEdit.style.marginRight = '3px';
                var buttonDelete = document.createElement("span");
                buttonDelete.className += 'detail-action action-delete text-danger fas fa-trash-alt';
                cellAction.appendChild(buttonEdit);
                cellAction.appendChild(buttonDelete);
                rowIndex++;
            }
            row.setAttribute('data-detail', '{"serviceID":{"id":' + serviceID + '},"quantity":' + quantity + '}');
            cellProduct.innerHTML = product;
            cellPrice.innerHTML = priceformated;
            cellQuantity.innerHTML = quantity;
            cellAmount.innerHTML = amount;
            resetDetailInput();
            calculateRepay();
        });



        $(document).on('click', '.detail-action', function () {
            if ($(this).hasClass('action-edit')) {
                editRow = $(this).parents('tr').get(0);
                var dataDetail = $(this).parents('tr').data('detail');
                var serviceID = dataDetail['serviceID']['id'];
                var quantity = dataDetail['quantity'];
                var amount = dataDetail['amount'];
                $('#serviceID option[value="' + serviceID + '"]').prop('selected', true);
                $('#quantity').val(quantity);
                var priceformated = $('#serviceID option:selected').data('priceformated');
                $('#price').val(priceformated);
                $('#amount').val(amount);
            } else if ($(this).hasClass('action-delete')) {
                $(this).parents('tr').nextAll('tr').each(function () {
                    var currentIndex = parseInt($(this).find('td:first-child').html());
                    $(this).find('td:first-child').html(currentIndex - 1);
                });
                $(this).parents('tr').remove();
                resetDetailInput();
                calculateRepay();
                rowIndex--;
            }
        });

        function getDetails() {
            var details = [];
            $('.detail').each(function (i, v) {
                var detail = $(this).data('detail');
                if (!details['exclude']) {
                    details.push(detail);
                }
            });
            return details;
        }
    </script>
    <script>
        $('#quantity').on('press keyup click', function () {
            ;
            var max = $('#serviceID option:selected').data('quantity');
            if ($('#quantity').val() > max) {
                openAlert(null, function () {
                    new Noty({theme: 'nest', text: 'Số lượng nhập vào quá lớn!', layout: 'topCenter', type: 'warning'}).show();
                });
                $('#quantity').val(max);
            }
            calculate();
        });
        $('#serviceID').on('change', function () {
            $('#price').val($('#serviceID option:selected').data('priceformated'));
            $('#quantity').val(1);
            $('#quantity').attr('max', $('#serviceID option:selected').data('quantity'));
            calculate();
        });
        function calculate() {
            var price = $('#serviceID option:selected').data('price');
            var quantity = $('#quantity').val();
            var amount = price * quantity;
            $('#amount').val(amount);
        }
        function resetDetailInput() {
            $('#serviceID option[value=""]').prop('selected', true);
            $('#price').val('0 đ');
            $('#quantity').val(1);
            $('#amount').val(0);
            var totalAmount = 0;
            $('.detail').each(function (i, v) {
                var detail = $(this).data('detail');
                totalAmount += detail.amount;
            });
            $('#totalAmount').val(totalAmount);
        }
    </script>

</div>