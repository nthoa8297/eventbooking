/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package systemenum;

/**
 *
 * @author Admin
 */
public enum AppStatus {
    DE, DC, UV, UA, DI, RJ, CA, RT;

    public static String deCodeAppStatus(AppStatus appStatus) {
        switch (appStatus) {
            case DE:
                return "Data Entry";
            case DC:
                return "Document Check";
            case UV:
                return "Under Verifycation";
            case UA:
                return "Under Approval";
            case DI:
                return "Disbursed";
            case RJ:
                return "Rejection";
            case CA:
                return "Cancellation";
            case RT:
                return "Return";
            default:
                return "Wrong application status";
        }
    }
}
