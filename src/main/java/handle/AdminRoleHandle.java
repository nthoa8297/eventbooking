package handle;

import entity.AdminRole;

public class AdminRoleHandle extends AbstractHandle {

    public AdminRoleHandle() {
        super(AdminRole.class);
    }
}
