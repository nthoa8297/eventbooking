package interceptor;

import entity.Admin;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import util.Alert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        request.getSession().setMaxInactiveInterval(60 * 15);
        Admin admin = (Admin) request.getSession().getAttribute("ADMIN");
        String uri = request.getRequestURI();
        if (admin == null) {
            if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))) {
                response.getWriter().println(Alert.createIntervalRedirect("Phiên làm việc hết hạn!", "Đăng nhập lại sau: ", "/Ebms/Login", 5000));
            } else {
                response.sendRedirect("/Ebms/Login");
            }
            return false;
        } else {
            if (uri.toUpperCase().endsWith("/EBMS")) {
                return true;
            } else {
                String[] strs = uri.split("/");
                if (admin.getAdminRoleID().getModuleInRole().contains(strs[2])) {
                    request.setAttribute("URL_ACTIVE", uri);
                    return true;
                } else {
                    System.out.println("/403 Forbidden: " + admin.getUsername() + " trying access to " + uri);
                    return false;
                }
            }
        }
    }
}
