/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handle;

import entity.EventInstance;
import entity.ServiceMapping;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Customer
 */
public class EventInstanceHandle extends AbstractHandle {

    public EventInstanceHandle() {
        super(EventInstance.class);
    }

    public List<EventInstance> getTOP5() {
        List<EventInstance> rs = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(EventInstance.class);
                cr.createAlias("addressID", "addressID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("addressID", FetchMode.JOIN);
                cr.createAlias("eventID", "eventID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("eventID", FetchMode.JOIN);
                cr.add(Restrictions.eq("status", 0));
//                cr.add(Restrictions.le("startDate", new Date()));
                cr.setMaxResults(5);
                rs = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }

    public List paging(Map map) {
        List list = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(EventInstance.class);
                cr.createAlias("addressID", "addressID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("addressID", FetchMode.JOIN);
//                cr.createAlias("serviceMappings", "serviceMappings", JoinType.LEFT_OUTER_JOIN).createAlias("serviceMappings.serviceID","serviceMappings.serviceID",JoinType.LEFT_OUTER_JOIN);
//                cr.setFetchMode("serviceMappings", FetchMode.JOIN).setFetchMode("serviceMappings.serviceID", FetchMode.JOIN);
                if (map.get("status") != null) {
                    cr.add(Restrictions.eq("status", (Integer) map.get("status")));
                }
                cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                Integer totalResult = cr.list().size();
                map.put("totalPage", totalResult % 10 > 0 ? Math.round(totalResult / 10 + 1) : totalResult / 10);
                cr.setFirstResult(((Integer) map.get("currentPage") - 1) * 10);
                cr.setMaxResults(10);
                cr.addOrder(Order.desc("createdDate"));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }
    public EventInstance findByNameAscii(String nameAscii) {
        EventInstance rs = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(EventInstance.class);
                cr.createAlias("addressID", "addressID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("addressID", FetchMode.JOIN);
                cr.createAlias("eventID", "eventID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("eventID", FetchMode.JOIN);
                cr.add(Restrictions.eq("nameAscii", nameAscii));
                rs = (EventInstance) cr.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }

    public List<EventInstance> findByEvent(List<Integer> lst) {
        List<EventInstance> rs = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(EventInstance.class);
                cr.createAlias("addressID", "addressID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("addressID", FetchMode.JOIN);
                cr.createAlias("eventID", "eventID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("eventID", FetchMode.JOIN);
                cr.add(Restrictions.in("eventID.id", lst));
                rs =  cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }

    public EventInstance save(EventInstance eventInstance) {
        Session session = null;
        Transaction tran = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                tran = session.beginTransaction();
                //save eventInstance
                if (eventInstance.getId() == null) {
                    session.save(eventInstance);
                } else {
                    session.update(eventInstance);
                }
                //insert ticket
                session.createSQLQuery("DELETE FROM Ticket WHERE EventInstanceID=:eventInstanceID")
                        .setParameter("eventInstanceID", eventInstance.getId()).executeUpdate();


                if (eventInstance.getNormalTicket() > 0 || eventInstance.getVipTicket() > 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("INSERT INTO TICKET(EventInstanceID,Price,IsGuests) values ");
                    for (int i = 0; i < eventInstance.getNormalTicket(); i++) {
                        sb.append(i == 0 ? "(" : ",(")
                                .append(eventInstance.getId())
                                .append(",")
                                .append(eventInstance.getTicketPrice())
                                .append(",")
                                .append("0")
                                .append(")");
                    }
                    if (eventInstance.getVipTicket() > 0) {
                        sb.append(",");
                    }
                    for (int i = 0; i < eventInstance.getVipTicket(); i++) {
                        sb.append(i == 0 ? "(" : ",(")
                                .append(eventInstance.getId())
                                .append(",")
                                .append("0")
                                .append(",")
                                .append("1")
                                .append(")");
                    }
                    System.out.println(sb.toString());
                    session.createSQLQuery(sb.toString()).executeUpdate();
                }

                //save ServiceMapping
                session.createSQLQuery("DELETE FROM ServiceMapping WHERE EventInstanceID=:eventInstanceID")
                        .setParameter("eventInstanceID", eventInstance.getId()).executeUpdate();
                for (ServiceMapping serviceMapping : eventInstance.getServiceMappings()) {
                    serviceMapping.setEventInstanceID(eventInstance.getId());
                    session.save(serviceMapping);
                }
                HibernateConfiguration.getInstance().commitTransaction(tran);
            }
        } catch (Exception e) {
            HibernateConfiguration.getInstance().rollbackTransaction(tran);
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return eventInstance;
    }

    public EventInstance findEager(Integer id) {
        EventInstance rs = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(EventInstance.class);
                cr.createAlias("addressID", "addressID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("addressID", FetchMode.JOIN);
                cr.createAlias("eventID", "eventID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("eventID", FetchMode.JOIN);
                cr.add(Restrictions.eq("id", id));
                rs = (EventInstance) cr.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }
    public List findByField(List<String> lst) {
        List rs = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(EventInstance.class);
                cr.createAlias("addressID", "addressID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("addressID", FetchMode.JOIN);
                cr.createAlias("eventID", "eventID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("eventID", FetchMode.JOIN);
                rs =  cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }
    public List<EventInstance> findByNameAscii_Event(String nameAscii) {
        List<EventInstance> rs = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(EventInstance.class);
                cr.createAlias("addressID", "addressID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("addressID", FetchMode.JOIN);
                cr.createAlias("eventID", "eventID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("eventID", FetchMode.JOIN);
                cr.add(Restrictions.eq("eventID.nameAscii", nameAscii));
                rs = (List<EventInstance>) cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }
    public Integer updateEventInstanceCaringIDs(Integer id, String eventInstanceCaringIDs) throws Exception {
        Session session = null;
        Integer result = 0;
        Transaction tran = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            tran = session.beginTransaction();
            if (session != null) {
                session.beginTransaction();
                result = session.createSQLQuery("update Customer set EventInstanceCaringIDs=:eventInstanceCaringIDs where ID=:id")
                        .setParameter("eventInstanceCaringIDs", eventInstanceCaringIDs)
                        .setParameter("id", id)
                        .executeUpdate();
                HibernateConfiguration.getInstance().commitTransaction(tran);
            }
        } catch (Exception e) {
            HibernateConfiguration.getInstance().rollbackTransaction(tran);
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
