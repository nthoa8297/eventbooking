package controller.client;

import entity.Customer;
import entity.Event;
import entity.EventInstance;
import handle.CustomerHandle;
import handle.EventHandle;
import handle.EventInstanceHandle;
import handle.FieldHandle;
import javafx.util.Pair;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import util.Alert;
import util.CustomFunction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Controller
public class HomeController {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String home(ModelMap mm) throws Exception {
        mm.put("AVAILABLE_EVENTINSTANCE", new EventInstanceHandle().getTOP5());
        List<Event> events =(List<Event>) new EventHandle().findAllAvailable();
        for(Event e : events){
            if( !StringUtils.isEmpty(e.getFieldIDs())
                    && e.getFieldIDs().length()>0){
                String[] arrFieldID = e.getFieldIDs().split(",");
                List<String> list = Arrays.asList(arrFieldID);
                List<Integer> integerList = list.stream()
                        .map(s -> Integer.parseInt(s))
                        .collect(Collectors.toList());
                e.setFieldIDs(new FieldHandle().getNameField(integerList));
            }
        }
        mm.put("LIST_EVENT", events);
        return "Home";
    }
//    @RequestMapping(value ="/{nameAscii:^(?!Ebms)[a-zA-Z0-9-]+}", method = RequestMethod.GET)
//    public String eventinstance(@PathVariable(value="nameAscii") String nameAscii,ModelMap mm)   {
//        System.out.println(nameAscii);
//        mm.put("SELECT_EVENTINSTANCE", new EventInstanceHandle().findByNameAscii(nameAscii));
//        return "Home.EventInstance";
//    }

    @RequestMapping(value = "/eventinstance/{nameAscii}", method = RequestMethod.GET)
    public String eventinstance(@PathVariable(value="nameAscii") String nameAscii,ModelMap mm) throws Exception {
        mm.put("LIST_EVENT",new EventHandle().findAllAvailable());
        mm.put("SELECT_EVENTINSTANCE", new EventInstanceHandle().findByNameAscii(nameAscii));
        return "Home.EventInstance";
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Pair login(@RequestBody Map map, HttpSession session) throws Exception {
        try {
            String redirectUrl = (String) map.get("r");
            Customer customer = new CustomerHandle().checkLogin((String) map.get("username"), CustomFunction.md5((String) map.get("password")));
            if (customer == null) {
                return new Pair(0, Alert.createErrorAlert("Sai tên tài khoản hoặc mật khẩu"));
            } else {
                session.setAttribute("CUSTOMER_CURRENT", customer);
                return new Pair(1, Alert.createRedirectPage(redirectUrl == null ? "/" : redirectUrl));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Đã có lỗi xảy ra!"));
        }
    }


    @RequestMapping(value = "/info/doimatkhau")
    public String info_doimatkhau() {
        return "Home.Info.D";
    }

    @RequestMapping(value = "/info/thongtintaikhoan")
    public String info_thongtintaikhoan(ModelMap mm) throws Exception {
        mm.put("LIST_FIELD", new FieldHandle().findAll());
        mm.put("LIST_EVENT", new EventHandle().findAllAvailable());
        return "Home.Info.T";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IllegalStateException {
        request.getSession().invalidate();
        try {
            response.sendRedirect("/");
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @RequestMapping(value = "/info/updatepassword", method = RequestMethod.POST)
    @ResponseBody
    public Pair Edit(@RequestBody Map mapCustomer, HttpServletRequest request) {
        Integer check = 0;
        try {
            Customer current = (Customer) request.getSession().getAttribute("CUSTOMER_CURRENT");
            if (current.getId() == null) {
                return new Pair(0, Alert.createErrorAlert("Bạn chưa đăng nhập!"));
            }

            String password_new = (String) mapCustomer.get("password_new");
            String password_confirm = (String) mapCustomer.get("password_confirm");
            if (password_new.equals(password_confirm)) {
                new CustomerHandle().changePassword(current.getId(), CustomFunction.md5(password_confirm));
                return new Pair(1, Alert.createIntervalRedirect("Đổi mật khẩu thành công!", "Chuyển về trang chủ sau ", "/",
                        200));
            }
            return new Pair(0, Alert.createErrorAlert("Mật khẩu nhập lại chưa đúng!"));
        } catch (ConstraintViolationException cve) {
            cve.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Đổi mật khẩu thất bại!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Lỗi đổi mật khẩu"));
        }
    }

    @RequestMapping(value = "/info/updateinfo", method = RequestMethod.POST)
    @ResponseBody
    public Pair updateinfo(@RequestBody Map mapCustomer, HttpServletRequest request) {
        try {
            Customer current = (Customer) request.getSession().getAttribute("CUSTOMER_CURRENT");
            if (current.getId() == null) {
                return new Pair(0, Alert.createErrorAlert("Bạn chưa đăng nhập!"));
            }
            Customer customer = (Customer) new CustomerHandle().getObjectFromMap(mapCustomer);
            customer.setId(current.getId());
            new CustomerHandle().update(customer);
            return new Pair(1, Alert.createSuccessAlert("Sửa thông tin khách hàng thành công!"));
        } catch (ConstraintViolationException cve) {
            cve.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Tài khoản đã tồn tại!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Sửa thông tin khách hàng thất bại"));
        }
    }
    @RequestMapping(value = "/register")
    public String register(ModelMap mm,HttpServletRequest request) throws Exception {
        mm.put("LIST_FIELD", new FieldHandle().findAll());
        mm.put("LIST_EVENT", new EventHandle().findAllAvailable());
        return  "Home.Register";
    }
    @RequestMapping(value = "/contact")
    public String contact(){
        return  "Home.Contact";
    }
    @RequestMapping(value = "/introduce")
    public String introduce(){
        return  "Home.Introduce";
    }

    @RequestMapping(value = "/register/insert", method = RequestMethod.POST)
    @ResponseBody
    public Pair insert(@RequestBody Map mapCustomer) {
        try {
            Customer customer = (Customer) new CustomerHandle().getObjectFromMap(mapCustomer);
            customer.setPassword(CustomFunction.md5(customer.getPassword()));
            new CustomerHandle().create(customer);
            return new Pair(1, Alert.createIntervalRedirect("Đăng ký thành công!", "Chuyển về trang chủ sau ", "/",
                    200));
        } catch (ConstraintViolationException cve) {
            cve.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Tài khoản đã tồn tại!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Đăng ký thất bại"));
        }
    }
    @RequestMapping(value = "/event")
    public String event(ModelMap mm) throws Exception {
        List<Event> events =(List<Event>) new EventHandle().findAllAvailable();
        for(Event e : events){
            if( !StringUtils.isEmpty(e.getFieldIDs())
                    && e.getFieldIDs().length()>0){
                String[] arrFieldID = e.getFieldIDs().split(",");
                List<String> list = Arrays.asList(arrFieldID);
                List<Integer> integerList = list.stream()
                        .map(s -> Integer.parseInt(s))
                        .collect(Collectors.toList());
                e.setFieldIDs(new FieldHandle().getNameField(integerList));
            }
        }
        mm.put("LIST_EVENT", events);
        return  "Home.Event";
    }
    @RequestMapping(value = "/event/{nameAscii}", method = RequestMethod.GET)
    public String event(@PathVariable(value="nameAscii") String nameAscii,ModelMap mm) throws Exception {
        mm.put("SELECT_EVENT", new EventHandle().findByNameAscii(nameAscii));
        return "Home.EventSelect";
    }

    @RequestMapping(value = "/events/{nameAscii}", method = RequestMethod.GET)
    public String events(@PathVariable(value = "nameAscii") String nameAscii, ModelMap mm) throws Exception {
        mm.put("LIST_EVENTINSTANCE", new EventInstanceHandle().findByNameAscii_Event(nameAscii));
        return "Home.EventInstance.By.Event";
    }

    @RequestMapping(value = "/eventsinstance", method = RequestMethod.GET)
    public String eventsinstance(ModelMap mm) throws Exception {
        mm.put("LIST_EVENTINSTANCE", new EventInstanceHandle().findAll());
        return "Home.EventsInstance";
    }
    @RequestMapping(value = "/eventsinstanceCare", method = RequestMethod.GET)
    public String eventsinstanceCare1( ModelMap mm,HttpServletRequest request,HttpServletResponse response) throws Exception {
        List<EventInstance> lst = new ArrayList<>();
        Customer customer = (Customer) request.getSession().getAttribute("CUSTOMER_CURRENT");
        mm.put("TITLE","Danh sách sự kiện bạn quan tâm");
        if(customer == null){
            mm.put("TITLE","Mời đăng nhập!");
            return "Home.EventsInstanceCare";
        }
        String[] arrFieldC = new String[]{};
        if (customer.getFieldCaringIDs().length() > 0) {
            arrFieldC = customer.getFieldCaringIDs().split(",");

            List<String> lstSFieldC = Arrays.asList(arrFieldC);
            List<Integer> lstIFieldC = lstSFieldC.stream()
                    .map(s -> Integer.parseInt(s))
                    .collect(Collectors.toList());


            List<Event> lstE = new EventHandle().findAllAvailable();
            List<Integer> eventID =new ArrayList<>();

            lstIFieldC.forEach(itemC -> {
                lstE.forEach(itemE -> {
                    String[] arrfieldE = new String[]{};
                    if (itemE.getFieldIDs().length() > 0) {
                        arrfieldE = itemE.getFieldIDs().split(",");
                        List<String> lstSFieldE = Arrays.asList(arrfieldE);
                        List<Integer> lstIFieldE = lstSFieldE.stream()
                                .map(s -> Integer.parseInt(s))
                                .collect(Collectors.toList());

                        lstIFieldE.forEach(itemFE ->{
                            if(itemC == itemFE){
                                eventID.add(itemE.getId());
                            }
                        });
                    }
                });
            });
            lst = new EventInstanceHandle().findByEvent(eventID);
            mm.put("LIST_EVENTINSTANCE",lst);
            return "Home.EventsInstanceCare";
        } else {
            lst = null;
            mm.put("LIST_EVENTINSTANCE",lst);
            mm.put("TITLE","Lĩnh vực bạn quan tâm chưa có sự kiện!");
            return "Home.EventsInstanceCare";
        }

    }

    @RequestMapping(value = "/like", method = RequestMethod.POST)
    @ResponseBody
    public Pair like(HttpServletRequest request, @RequestBody Map mapCustomer) {
        Integer check = 0;
        String eventID = String.valueOf(mapCustomer.get("eventID"));
        String[] arrStr = new String[]{};
        String delim = ",";
        String strConvert = "";
        try {
            Customer customer = (Customer) request.getSession().getAttribute("CUSTOMER_CURRENT");
            if(customer == null){
                return new Pair(0, Alert.createErrorAlert("Mời đăng nhập"));
            }

            if(String.valueOf(mapCustomer.get("like")).length()>0
                    && String.valueOf(mapCustomer.get("like")).equalsIgnoreCase("Quan tâm")){
                if(customer.getEventInstanceCaringIDs() != null
                        && customer.getEventInstanceCaringIDs().length()>0){
                    arrStr = customer.getEventInstanceCaringIDs().split(",");
                }
                List<String> list = new LinkedList<String>(Arrays.asList(arrStr));
                list.add(eventID);
                strConvert = String.join(delim,list);
                check = new EventInstanceHandle().updateEventInstanceCaringIDs(customer.getId(),strConvert);
                if(check==1){
                    ((Customer) request.getSession().getAttribute("CUSTOMER_CURRENT")).setEventInstanceCaringIDs(strConvert);
                }
                return new Pair(1, Alert.createSuccessAlert("Quan tâm thành công!"));
            }else{
                if(customer.getEventInstanceCaringIDs() != null
                        && customer.getEventInstanceCaringIDs().length()>0){
                    String[] eventInstanceCaringIDs= ((Customer) request.getSession().getAttribute("CUSTOMER_CURRENT")).getEventInstanceCaringIDs().split(",");
                    List<String> list = new LinkedList<String>(Arrays.asList(eventInstanceCaringIDs));
                    for(int i =0 ; i < list.size();i++){
                        if(list.get(i).equals(eventID)){
                            list.remove(i);
                        }
                    }
                    strConvert = String.join(delim,list);
                    check = new EventInstanceHandle().updateEventInstanceCaringIDs(customer.getId(),strConvert);
                    if(check==1){
                        ((Customer) request.getSession().getAttribute("CUSTOMER_CURRENT")).setEventInstanceCaringIDs(strConvert);
                        return new Pair(2, Alert.createSuccessAlert("Bỏ quan tâm thành công!"));
                    }
                }
            }
            return new Pair(0, Alert.createErrorAlert("Danh sách sự kiện trống!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Quan tâm thất bại"));
        }
    }
}

