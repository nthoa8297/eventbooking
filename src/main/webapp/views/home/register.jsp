<%-- 
    Document   : roomtype
    Created on : Jan 12, 2020, 11:41:20 AM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/WEB-INF/tlds/functions" prefix="f" %>


<!--================ PAGE-COVER =================-->
<style type="text/css">
    select, ul {
        height: 100px;
        overflow: auto;
        width: 100px;
    }

    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow-x: hidden;
        width: auto;
    }

    li {
        margin: 0;
        padding: 0;
    }

    label {
        display: block;
        color: WindowText;
        background-color: Window;
        margin: 0;
        padding: 0;
        width: 100%;
    }

    label:hover {
        background-color: Highlight;
        color: HighlightText;
    }
</style>
<div class="living_middle">
    <div class="container" style="background-color: white">
        <div class="col-md-12 wow fadeInRight animated" data-wow-delay="0.4s">
            <div class="educate_grid">
                <div class="col-md-12">
                    <div class="living_box">
                        <div class="img-responsive">
                            <form id="my-form" action="/register/insert">
                                <div class="form-group">
                                    <label>Tên khách hàng</label>
                                    <input name="name" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Tài khoản</label>
                                    <input name="username" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Mật khẩu</label>
                                    <input name="password" type="password" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Ngày sinh</label>
                                    <input id="dateOfBirth"
                                           value="${f:customFormatDate('dd/MM/yyyy',CUSTOMER_CURRENT.dateOfBirth)}"
                                           class="form-control datetimepicker"
                                           required>
                                </div>

                                <div class="form-group">
                                    <label>Lĩnh vực quan tâm</label>
                                    <ul>
                                        <c:forEach items="${LIST_FIELD}" var="item">
                                            <li class="custom-control custom-checkbox">
                                                <label for="field${item.id}" class="form-check-label">
                                                    <input type="checkbox" value="${item.id}"
                                                           name="fieldCaringIDs" id="field${item.id}"
                                                           class="custom-control-input">
                                                    <span style="font-weight: normal;"
                                                          class="custom-control-label"
                                                          for="field${item.id}">${item.name}</span>
                                                </label>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Huỷ</button>
                                    <button type="submit" class="btn btn-primary">Lưu</button>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
        $('#my-form').validate({
            submitHandler: function () {
                var data = $('#my-form').serializeObject();
                if ($('#dateOfBirth').val()) {
                    data.dateOfBirth = new Date(moment($('#dateOfBirth').val(), 'DD/MM/YYYY').format('YYYY/MM/DD'));
                }
                var fields = [];
                $.each($("input[name='fieldCaringIDs']:checked"), function () {
                    fields.push($(this).val());
                });

                data.fieldCaringIDs = fields.join(",");
                var url = $('#my-form').attr('action');

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    success: function (result) {
                        openAlert(result.value);
                    },
                    error: function () {
                        errorAlert();
                    }
                });
            }
        });
        var firstOpen;
        $('.datetimepicker').datetimepicker({
            viewMode: 'years',
            format: 'DD/MM/YYYY',
            icons: {
                previous: 'fas fa-angle-left',
                next: 'fas fa-angle-right',
            }
        }).on("dp.show", function () {
            if (typeof firstOpen === 'undefined') {
                $(this).data('DateTimePicker').date("01/01/1997");
                firstOpen = false;
            }
        });
    });
</script>