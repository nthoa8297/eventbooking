/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import javax.persistence.*;
import java.util.List;

/**
 *
 * @author 1997n
 */
@Entity
@Table(name = "ServiceMapping")
public class ServiceMapping {

    private Integer id;
    private Integer eventInstanceID;
    private Service serviceID;
    private Integer quantity;

    @Id
    @Column(name = "ID", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "EventInstanceID")
    public Integer getEventInstanceID() {
        return eventInstanceID;
    }

    public void setEventInstanceID(Integer eventInstanceID) {
        this.eventInstanceID = eventInstanceID;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ServiceID")
    public Service getServiceID() {
        return serviceID;
    }

    public void setServiceID(Service serviceID) {
        this.serviceID = serviceID;
    }


    @Column(name = "Quantity")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
