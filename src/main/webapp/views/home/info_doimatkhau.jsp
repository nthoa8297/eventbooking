<%-- 
    Document   : roomtype
    Created on : Jan 12, 2020, 11:41:20 AM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="/WEB-INF/tlds/functions" prefix="f" %>
<!--================== PAGE-COVER ===============-->

<div class="living_middle">
    <div class="container">
        <div class="col-md-3 wow fadeInLeft" data-wow-delay="0.4s">
            <br>
            <a class="feature" href="/info/thongtintaikhoan">
                <ul class="feature" >
                    <li><i class=""></i></li>
                    <li class="feature_right"><h4>Thông tin tài khoản</h4>
                        <p></p>
                    </li>
                    <div class="clearfix"></div>
                </ul>
            </a>
            <a class="feature" href="/info/doimatkhau">
                <ul class="feature"  >
                    <li><i class=""></i></li>
                    <li class="feature_right"><h4>Đổi mật khẩu</h4>
                        <p></p>
                    </li>
                    <div class="clearfix"></div>
                </ul>
            </a>
<%--            <a class="feature" href="/info/eventinstancecare">--%>
<%--                <ul class="feature" >--%>
<%--                    <li><i class=""></i></li>--%>
<%--                    <li class="feature_right"><h4>Quan tâm</h4>--%>
<%--                        <p></p>--%>
<%--                    </li>--%>
<%--                    <div class="clearfix"></div>--%>
<%--                </ul>--%>
<%--            </a>--%>
            <a class="feature" href="/logout">
                <ul class="feature">
                    <li><i class=""></i></li>
                    <li class="feature_right"><h4>Thoát</h4>
                        <p></p>
                    </li>
                    <div class="clearfix"></div>
                </ul>
            </a>
        </div>
        <div class="col-md-9 wow fadeInRight animated" data-wow-delay="0.4s"
             style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
            <div class="educate_grid">
                <div class="col-md-12">
                    <div class="living_box">
                        <div class="img-responsive">
                            <form id="my-form"  action="/info/updatepassword">
                                <div class="form-group">
                                    <label>Mật khẩu mới</label>
                                    <input name="password_new" class="form-control" type="password"
                                           class="form-control"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label>Xác nhận mật khẩu mới</label>
                                    <input name="password_confirm" type="password"
                                           class="form-control" required>
                                </div>


                            <div class="living_desc desc1">
                                <button class="btn btn-success" type="submit">Xác nhận</button>
                                <div class="clearfix"></div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    <%--var text = "${pageContext.request.contextPath}/${SELECT_EVENTINSTANCE.nameAscii}";--%>
    <%--$('#qrcode').qrcode({--%>
    <%--    "render": "div",--%>
    <%--    "width": 50,--%>
    <%--    "height": 50,--%>
    <%--    "color": "#3a3",--%>
    <%--    "text":text--%>
    <%--});--%>
</script>

<script>

    $(document).ready(function () {
        $('#my-form').validate({
            submitHandler: function () {
                var data = $('#my-form').serializeObject();
                var url = $('#my-form').attr('action');
debugger
                $.ajax({
                    url: url ,
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    success: function (result) {
                        openAlert(result.value);
                        if(result.key==1){
                            window.location.href = "/logout";
                        }
                    },
                    error: function () {
                        errorAlert();
                    }
                });
            }
        });
    });



</script>