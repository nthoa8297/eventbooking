package handle;

import entity.Admin;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

public class AdminHandle extends AbstractHandle {

    public AdminHandle() {
        super(Admin.class);
    }

    public Admin checkLogin(String username, String password) {
        Admin admin = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Admin.class);
                cr.createAlias("adminRoleID", "adminRoleID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("adminRoleID", FetchMode.JOIN);
                cr.add(Restrictions.eq("username", username));
                cr.add(Restrictions.eq("password", password));
                admin = (Admin) cr.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return admin;
    }

    public Admin findEager(Integer id) {
        Admin admin = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Admin.class);
                cr.createAlias("adminRoleID", "adminRoleID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("adminRoleID", FetchMode.JOIN);
                cr.add(Restrictions.eq("id", id));
                admin = (Admin) cr.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return admin;
    }

    public Integer changePassword(Integer id, String password) throws Exception {
        Session session = null;
        Integer result = 0;
        Transaction tran = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            tran = session.beginTransaction();
            if (session != null) {
                session.beginTransaction();
                result = session.createSQLQuery("update Admin set Password=:password where Id=:id")
                        .setParameter("password", password)
                        .setParameter("id", id)
                        .executeUpdate();
                HibernateConfiguration.getInstance().commitTransaction(tran);
            }
        } catch (Exception e) {
            HibernateConfiguration.getInstance().rollbackTransaction(tran);
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
