<%-- 
    Document   : roomtype
    Created on : Jan 12, 2020, 11:41:20 AM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri ="/WEB-INF/tlds/functions" prefix="f"%>

<!--========== PAGE-COVER =========-->
<section class="page-cover dashboard">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">My Account</h1>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">My Account</li>
                </ul>
            </div><!-- end columns -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end page-cover -->


<!--===== INNERPAGE-WRAPPER ====-->
<section class="innerpage-wrapper">
    <div id="dashboard" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="dashboard-heading">
                        <h2>HỒ <span>SƠ</span></h2>
                        <p>Chào ${CUSTOMER.name},!</p>
                        <p>Những thông tin mà bạn cung cấp cho chúng tôi sẽ hiển thị ở đây.
                            Gần đây, bạn có thông tin gì thay đổi không có gì thay đổi không?
                            nếu có, cập nhật cho chúng tôi biết nhé!</p>
                    </div><!-- end dashboard-heading -->

                    <div class="dashboard-wrapper">
                        <div class="row">

                            <div class="col-xs-12 col-sm-2 col-md-2 dashboard-nav">
                                <ul class="nav nav-tabs nav-stacked text-center">
                                    <li class="active"><a href="/profile"><span><i class="fa fa-user"></i></span>Hồ sơ</a></li>
                                    <li><a href="/room-order-history?s=0"><span><i class="fa fa-briefcase"></i></span>Lịch sử đặt phòng</a></li>
                                    <li><a href="/bill"><span><i class="fas fa-file-invoice-dollar"></i></span>Hoá đơn</a></li>
                                    <li><a href="#"><span><i class="fa fa-heart"></i></span>Wishlist</a></li>
                                    <li><a href="#"><span><i class="fa fa-credit-card"></i></span>My Cards</a></li>
                                </ul>
                            </div><!-- end columns -->

                            <div class="col-xs-12 col-sm-10 col-md-10 dashboard-content user-profile">
                                <h2 class="dash-content-title">My Profile</h2>
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h4>Profile Details</h4></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12  user-detail">
                                                <form id="form-profile" novalidate action="/profile" method="POST">
                                                    <div class="form-group">
                                                        <label class="font-weight-light">Họ và tên</label>
                                                        <input value="${CUSTOMER.name}" type="text" class="form-control" placeholder="Họ và tên" name="name" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="font-weight-light">Số chứng minh thư nhân dân hoặc căn cước công dân</label>
                                                        <input value="${CUSTOMER.idCardNumber}" type="text" class="form-control" placeholder="Số chứng minh thư nhân dân hoặc căn cước công dân" name="idCardNumber" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="font-weight-light">Số điện thoại</label>
                                                        <input value="${CUSTOMER.phoneNumber}" type="text" class="form-control" placeholder="Số điện thoại" name="phoneNumber" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="font-weight-light">Giới tính</label>
                                                        <select name="gender" class="form-control">
                                                            <option value="">Chọn giới tính</option>
                                                            <option ${CUSTOMER.gender==1?'selected':''} value="1">Nam</option>
                                                            <option ${CUSTOMER.gender==0?'selected':''} value="0">Nữ</option>
                                                            <option ${CUSTOMER.gender==2?'selected':''} value="2">Khác</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="font-weight-light">Ngày sinh</label>
                                                        <input value="${f:customFormatDate('dd/MM/yyyy',CUSTOMER.dateOfBirth)}" type="text" class="form-control dpd3" placeholder="Ngày sinh" required id="dateOfBirth">
                                                    </div>
                                                    <button type="submit" class="btn">Chỉnh sửa</button>
                                                </form>
                                            </div><!-- end columns -->
                                        </div><!-- end row -->

                                    </div><!-- end panel-body -->
                                </div><!-- end panel-detault -->
                            </div><!-- end columns -->

                        </div><!-- end row -->
                    </div><!-- end dashboard-wrapper -->
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->          
    </div><!-- end dashboard -->
</section><!-- end innerpage-wrapper -->
<script>
    $(document).ready(function () {
        $('#form-profile').validate({
            submitHandler: function () {
                ;
                var data = $('#form-profile').serializeObject();
                var url = $('#form-profile').attr('action');
                if ($('#dateOfBirth').val()) {
                    data.dateOfBirth = new Date(moment($('#dateOfBirth').val(), 'DD/MM/YYYY').format('YYYY/MM/DD'));
                }
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    success: function (result) {
                        openAlert(result.value);
                    },
                    error: function () {
                        errorAlert();
                    }
                });
            }
        });
    });
</script>