package controller.admin;

import entity.Address;
import handle.AddressHandle;
import javafx.util.Pair;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import util.Alert;

import java.util.Map;

@Controller
@RequestMapping("/Ebms/Address")
public class AddressController {

    @RequestMapping("")
    public String Address(ModelMap mm) throws Exception {
        mm.put("LIST_ITEM", new AddressHandle().findAllAvailable());
        return "Admin.Address";
    }

    @RequestMapping(value = "/Content", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView AddressContent(ModelMap mm) throws Exception {
        mm.put("LIST_ITEM", new AddressHandle().findAllAvailable());
        return new ModelAndView("Admin.Address.Content");
    }

    @RequestMapping(value = "/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView viewInsert() {
        return new ModelAndView("Admin.Address.ViewInsert");
    }

    @RequestMapping(value = "/Insert", method = RequestMethod.POST)
    @ResponseBody
    public Pair insert(@RequestBody Map mapAddress) {
        try {
            Address Address = (Address) new AddressHandle().getObjectFromMap(mapAddress);

            new AddressHandle().create(Address);
            return new Pair(1, Alert.createSuccessAlert("Thêm địa chỉ thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Thêm địa chỉ thất bại"));
        }
    }

    @RequestMapping(value = "/ViewEdit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewEdit(@PathVariable(value = "id") Integer id, ModelMap mm) throws Exception {
        mm.put("SELECTED_ITEM", new AddressHandle().findById(id));
        return new ModelAndView("Admin.Address.ViewEdit");
    }

    @RequestMapping(value = "/Edit", method = RequestMethod.POST)
    @ResponseBody
    public Pair Edit(@RequestBody Map mapAddress) {
        try {
            Address Address = (Address) new AddressHandle().getObjectFromMap(mapAddress);
            new AddressHandle().update(Address);
            return new Pair(1, Alert.createSuccessAlert("Sửa thông tin địa chỉ thành công!"));
        } catch (ConstraintViolationException cve) {
            cve.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Tài khoản đã tồn tại!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Sửa thông tin địa chỉ thất bại"));
        }
    }

    @RequestMapping(value = "/Delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Pair deleteAdminRole(@PathVariable(value = "id") Integer id) {
        try {
            new AddressHandle().delete(id);
            return new Pair(1, Alert.createSuccessAlert("Xoá địa chỉ thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Xoá địa chỉ thất bại"));
        }
    }
}
