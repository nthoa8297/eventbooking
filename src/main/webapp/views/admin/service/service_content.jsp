<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<c:choose>
    <c:when test="${f:size(LIST_ITEM)==0}">
        <span class="alert alert-danger">Không có bản ghi nào tồn tại!</span>
    </c:when>
    <c:otherwise>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 1%">STT</th>
                        <th>Tên dịch vụ</th>
                        <th>Giá</th>
                        <th>Mô tả</th>
                        <th>Ngày tạo</th>
                        <th>Nhà cung cấp</th>
                        <th style="width: 40px"></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${LIST_ITEM}" var="item" varStatus="index">
                        <tr>
                            <td>${index.index+1}</td>
                            <td>${item.name}</td>
                            <td>${f:customFormatDecimal('###,### đ',item.price)}</td>
                            <td>${item.description}</td>
                            <td>${f:customFormatDate("dd/MM/yyyy hh:mm", item.createdDate)}</td>
                            <td>${item.providerID.id}-${item.providerID.name}</td>
                            <td style="white-space: nowrap;">
                                <a title="Sửa" class="btn-open-modal"  href="javascript:void(0)" 
                                   data-controller="/Ebms/Service/ViewEdit/${item.id}"><i class="fas fa-edit text-primary"></i></a>
                                <a title="Xoá" class="btn-send-ajax" href="javascript:void(0)" 
                                   data-controller="/Ebms/Service/Delete/${item.id}"><i class="fas fa-trash-alt text-danger"></i></a>     
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </c:otherwise>
</c:choose>

