$(document).on('show.bs.dropdown', '.table-responsive', function () {
    $(this).css("overflow", "inherit");
});
$(document).on('hide.bs.dropdown', '.table-responsive', function () {
    $(this).css("overflow", "auto");
});
$(document).on('hide.bs.modal', '.modal:not(.not-auto-remove)', function () {
    $(this).remove();
});
$(document).on('hidden.bs.modal', '.modal:not(.not-auto-remove)', function () {
    $(this).remove();
});
function reloadAjaxContent() {
    debugger
    var url = $('#reloadController').val();
    sendAjax(url, 'GET', null, function (data) {
        $('#ajax-content').html(data);
    });
}
$(document).on('click', '.btn-open-modal', function (e) {
    e.preventDefault();
    var controller = $(this).data('controller');
    if (typeof controller !== "undefined") {
        sendAjax(controller, 'GET', null, function (data) {
            $('body').append(data);
            $('#myModal').modal({show: true});
        });
    }
});
$(document).on('click', '.btn-send-ajax', function () {
    var url = '';
    url = $(this).data('controller');
    if ($(this).hasClass('js-dialog-confirm-target')) {
        openDialogConfirmTarget(function () {
            sendAjax(url, 'GET', null, function (data) {
                openAlert(data.value);
                reloadAjaxContent();
            });
        });
    } else if (url.toUpperCase().includes('DELETE')) {
        Noty.closeAll();
        var n = new Noty({
            theme: 'nest',
            text: 'Xác nhận xoá ?',
            type: 'warning',
            layout: 'topCenter',
            buttons: [
                Noty.button('Đồng ý', 'btn btn-default waves-effect pull-left', function () {
                    sendAjax(url, 'GET', null, function (data) {
                        openAlert(data.value);
                        reloadAjaxContent();
                    });
                }, {id: 'button1', 'data-status': 'ok'}),
                Noty.button('Huỷ', 'btn btn-default waves-effect pull-right', function () {
                    n.close();
                })
            ]
        });
        n.show();
    } else {
        sendAjax(url, 'GET', null, function (data) {
            openAlert(data.value);
            reloadAjaxContent();
        });
    }
});
//$(document).on('change', '.btn-change-display-per-page', function () {
//    var url = $(this).data('controller');
//    url += $(this).val();
//    sendAjax(url, 'GET', null, function (data) {
//        $('#ajax-content').html(data);
//    });
//});

function sendAjax(url, type, data, handle) {
    $.ajax({
        url: url,
        type: type,
        data: data,
        statusCode: {
            405: function (response) {
                openAlert("<script>new Noty({theme: 'nest', text: '405 - Method not allowed!', layout: 'topCenter', type: 'error'}).show();</script>", function () {
                });
            },
            404: function (response) {
                openAlert("<script>new Noty({theme: 'nest', text: '404 - Not found!', layout: 'topCenter', type: 'error'}).show();</script>", function () {
                });
            }
        }, success: function (data) {
            if (typeof handle !== "undefined") {
                handle(data);
            }
        }
    });
}

function openAlert(data, callback) {
    Noty.closeAll();
    $('body').append(data);
    if (typeof callback === 'function') {
        callback();
    }
}
function errorAlert() {
    Noty.closeAll();
    var n = new Noty({theme: 'nest', text: '<p style="color:white">' + 'Đã xảy ra lỗi! Vui lòng thử lại sau!' + '</p>', layout: 'topCenter', type: 'error'}).show();
    setTimeout(function () {
        n.close();
        removeJsTagNoty();
    }, 3000);
}
function createErrorNoty(content) {
    Noty.closeAll();
    var n = new Noty({theme: 'nest', text: '<p style="color:white">' + content + '</p>', layout: 'topCenter', type: 'error'}).show();
    setTimeout(function () {
        n.close();
        removeJsTagNoty();
    }, 3000);
}

function removeUnicode(str) {
    str = str.toUpperCase();
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    str = str.replace(/[^A-Za-z0-9]/g, "_");
    return str;
}
function removeUnicodeURL(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/[^A-Za-z0-9]/g, "-");
    str = str.replace(/-+-/g, "-");
    str = str.replace(/^\-+|\-+$/g, "");
    return str;
}
function removeJsTagNoty() {
    var scripts = document.getElementsByTagName('script');
    var i = scripts.length;
    while (i--) {
        if ($(scripts[i]).hasClass('js-tag-noty')) {
            scripts[i].parentNode.removeChild(scripts[i]);
        }
    }
}