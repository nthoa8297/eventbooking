<%-- 
    Document   : top-bar
    Created on : Jan 12, 2020, 9:48:42 AM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--============= TOP-BAR ===========-->
<div id="top-bar" class="tb-text-grey">
    <div class="container">
        <div class="row">          
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div id="info">
                    <ul class="list-unstyled list-inline">
                        <li><span><i class="fa fa-map-marker"></i></span>FPT Polytechnic</li>
                        <li><span><i class="fa fa-phone"></i></span>+00 123 9999</li>
                    </ul>
                </div><!-- end info -->
            </div><!-- end columns -->

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div id="links">
                    <ul class="list-unstyled list-inline">
                        <c:choose>
                            <c:when test="${empty CUSTOMER}">
                                <li><a id="link-login" href="/login"><span><i class="fa fa-lock"></i></span>Đăng nhập</a></li>
                                <li><a href="/register"><span><i class="fa fa-plus"></i></span>Đăng ký</a></li>
                                            </c:when>
                                            <c:otherwise>
                                <li><a href="/profile"><span><i class="fas fa-user-alt"></i></span>${CUSTOMER.name}</a></li>
                                <li><a href="/logout"><span><i class="fas fa-sign-out-alt"></i></span>Đăng xuất</a></li>
                                            </c:otherwise>
                                        </c:choose>
                    </ul>
                </div><!-- end links -->
            </div><!-- end columns -->				
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end top-bar -->