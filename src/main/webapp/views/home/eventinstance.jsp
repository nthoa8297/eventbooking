<%-- 
    Document   : roomtype
    Created on : Jan 12, 2020, 11:41:20 AM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="/WEB-INF/tlds/functions" prefix="f" %>
<!--================== PAGE-COVER ===============-->
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0"></script>
<div class="living_middle">
    <div class="container">
        <div class="col-md-3 wow fadeInLeft" data-wow-delay="0.4s">
            <br>
            <c:forEach var="event" items="${LIST_EVENT}">
                <a class="feature" href="/events/${event.nameAscii}">
                    <ul class="feature">
                        <li><i class=""></i></li>
                        <li class="feature_right"><h4>${event.name}</h4>
                            <p></p>
                        </li>
                        <div class="clearfix"></div>
                    </ul>
                </a>
            </c:forEach>
        </div>
        <div class="col-md-9 wow fadeInRight animated" data-wow-delay="0.4s"
             style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
            <div class="educate_grid">
                <div class="col-md-12">
                    <div class="living_box">
                        <div class="img-responsive">
                            <div class="row">
                            <div class="col-12" >

                                <div id="aa" class="eventinstance" style="margin: 10px;">
                                    <style>
                                        .aa a img{
                                            width:100%;
                                        }
                                    </style>
                                    ${SELECT_EVENTINSTANCE.description}
                                </div>
                            </div>
                            </div>

                            <div class="living_desc desc1">
                                    <h3>
                                        <a href="javascript:void(0)" readonly="true" id="like">${f:isStringContainInteger(CUSTOMER_CURRENT.eventInstanceCaringIDs,SELECT_EVENTINSTANCE.id)==true?'Đã quan tâm':'Quan tâm'}</a>
                                    </h3>
                                <p><a href="/events/${SELECT_EVENTINSTANCE.eventID.nameAscii}"></a>${SELECT_EVENTINSTANCE.eventID.name}</p>
                                <p class="educate"  id="qrcode">

                                    <img style="width: 100px;height: 100px" src="https://api.qrserver.com/v1/create-qr-code/?size=50x50&data=${pageContext.request.contextPath}/${SELECT_EVENTINSTANCE.nameAscii}" alt="API tạo QR đang bận!"/>
                                </p>
                                <p class="price pr_box">${SELECT_EVENTINSTANCE.title}</p>
                                <p id="countdown"
                                   class="price pr_box"
<%--                                   style="position: relative;left: 234px;top: 34px;transition: none 0s ease 0s;cursor: move;font-size: 15px;" --%>
                                >
                                        ${f:countDownTime(SELECT_EVENTINSTANCE.startDate,SELECT_EVENTINSTANCE.status)}
                                </p>
                              <div class="price pr_box" id="fb-root"   >
                                <div class="fb-share-button"
                                     data-href="https://developers.facebook.com/docs/plugins/"
                                     data-layout="button_count" data-size="large">
                                        <a target="_blank"
                                           href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                                           class="fb-xfbml-parse-ignore">Chia sẻ
                                        </a>
                                </div>
                              </div>


                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        if($('#countdown').html()!='0'){
            $('#countdown').text('Sự kiện còn '+$('#countdown').html()+ ' ngày!');
        }else{
            $('#countdown').attr('hidden',true)
        }
        $("#like").on("click", function () {
            debugger
            var data = {};
            data.eventID = ${SELECT_EVENTINSTANCE.id};
            data.like = $("#like").html();
            $.ajax({
                url: "/like",
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function (result) {
                    debugger
                    openAlert(result.value);
                    if (result.key == 1) {
                        $("#like").text("Đã quan tâm");
                    } else if(result.key == 2){
                        $("#like").text("Quan tâm");
                    }
                },
                error: function () {
                    errorAlert();
                }
            });
        })
            $("title").text('${SELECT_EVENTINSTANCE.title} by .:DUHOOT:.')
    });


</script>