<%-- 
    Document   : roomtype
    Created on : Jan 12, 2020, 11:41:20 AM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri ="/WEB-INF/tlds/functions" prefix="f"%>

<!--========== PAGE-COVER =========-->
<section class="page-cover dashboard">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">My Account</h1>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">My Account</li>
                </ul>
            </div><!-- end columns -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end page-cover -->


<!--===== INNERPAGE-WRAPPER ====-->
<section class="innerpage-wrapper">
    <div id="dashboard" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="dashboard-heading">
                        <h2>HOÁ <span>ĐƠN</span></h2>
                        <p>Chào ${CUSTOMER.name}!</p>
                    </div><!-- end dashboard-heading -->
                    <div class="dashboard-wrapper">
                        <div class="row">
                            <div class="col-xs-12 col-sm-2 col-md-2 dashboard-nav">
                                <ul class="nav nav-tabs nav-stacked text-center">
                                    <li><a href="/profile"><span><i class="fa fa-user"></i></span>Hồ sơ</a></li>
                                    <li><a href="/room-order-history?s=0"><span><i class="fa fa-briefcase"></i></span>Lịch sử đặt phòng</a></li>
                                    <li class="active"><a href="/bill"><span><i class="fas fa-file-invoice-dollar"></i></span>Hoá đơn</a></li>
                                    <li><a href="#"><span><i class="fa fa-heart"></i></span>Wishlist</a></li>
                                    <li><a href="#"><span><i class="fa fa-credit-card"></i></span>My Cards</a></li>
                                </ul>
                            </div><!-- end columns -->
                            <div class="col-xs-12 col-sm-10 col-md-10 dashboard-content">
                                <div class="dashboard-listing recent-activity">
                                    <h3 class="dash-listing-heading">Hoá đơn của bạn</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <tbody>
                                                <c:forEach items="${LIST_ITEM}" var="item">
                                                    <tr>
                                                        <td class="dash-list-icon invoice-icon"><i class="fa fa-bars"></i></td>
                                                        <td class="dash-list-text invoice-text">
                                                            <h4 class="invoice-title">Hoá đơn số #${item.id}</h4>
                                                            <c:choose>
                                                                <c:when test="${item.status==0}">
                                                                    <label class="label label-danger">Thanh toán không khả dụng</label>
                                                                </c:when>
                                                                <c:when test="${item.status==1}">
                                                                    <c:if test="${item.isPaid}">
                                                                        <label class="label label-success">Đã thanh toán</label>
                                                                    </c:if>
                                                                    <c:if test="${!item.isPaid}">
                                                                        <label class="label label-warning">Chờ thanh toán</label>
                                                                    </c:if>
                                                                </c:when>
                                                            </c:choose>
                                                            <c:forEach items="${item.billDetails}" var="detail">
                                                                <ul class="list-unstyled list-inline invoice-info">
                                                                    <c:choose>
                                                                        <c:when test="${detail.isService}">
                                                                            <li class="invoice-status red">Dịch vụ: ${detail.serviceID.name}</li>
                                                                            <li class="invoice-order">${f:customFormatDecimal('###,### đ',detail.price)}/ lượt</li>
                                                                            <li class="invoice-order">Số lượt: ${detail.quantity} lượt</li>
                                                                            <li class="invoice-order">Tổng: ${f:customFormatDecimal('###,### đ',detail.price*detail.quantity)}</li>
                                                                            <li class="invoice-date text-muted">| ${f:customFormatDate('dd/MM/yyyy hh:mm',detail.createdDate)}</li>
                                                                            <li class="invoice-order">
                                                                                <c:if test="${detail.status==0}">
                                                                                    <label class="label label-warning">Chờ thực hiện</label>
                                                                                </c:if>
                                                                                <c:if test="${detail.status==1}">
                                                                                    <label class="label label-success">Hoàn thành</label>
                                                                                </c:if>
                                                                            </li>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <li class="invoice-status red">Phòng : ${detail.roomID.name}</li>
                                                                            <li class="invoice-order">${f:customFormatDecimal('###,### đ',detail.price)}/ngày & đêm</li>
                                                                            <li class="invoice-order">${detail.quantity} ngày</li>
                                                                            <li class="invoice-order">Tổng: ${f:customFormatDecimal('###,### đ',detail.price*detail.quantity)}</li>
                                                                            <li class="invoice-date text-muted">| ${f:customFormatDate('dd/MM/yyyy hh:mm',detail.createdDate)}</li>
                                                                            <li class="invoice-order">
                                                                                <c:if test="${detail.status==0}">
                                                                                    <label class="label label-warning">Đang sử dụng</label>
                                                                                </c:if>
                                                                                <c:if test="${detail.status==1}">
                                                                                    <label class="label label-success">Đã trả phòng</label>
                                                                                </c:if>
                                                                            </li>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </ul>      
                                                            </c:forEach>
                                                        </td>
                                                        <td class="dash-list-btn">
                                                            <c:if test="${!item.isPaid && item.status==1}">
                                                                <button id="btn-payment" data-bill-id="${item.id}" class="btn btn-orange">Thanh toán</button>
                                                            </c:if>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div><!-- end table-responsive -->
                                </div><!-- end recent-activity -->
                            </div><!-- end columns -->
                        </div>
                    </div><!-- end dashboard-wrapper -->
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->          
    </div><!-- end dashboard -->
</section><!-- end innerpage-wrapper -->

<script>
    $(function () {
        $('#btn-payment').on('click', function () {
            var billId = $(this).data('bill-id');
            $.ajax({
                url: '/vnpay-direct?bid=' + billId,
                type: 'GET',
                data: null,
                contentType: 'application/json',
                success: function (data) {
                    $('body').append(data.value);
                }, error: function () {
                    errorAlert();
                }
            });
        });
    });
</script>