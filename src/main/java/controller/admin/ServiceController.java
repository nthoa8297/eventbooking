/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import controller.*;
import entity.Service;
import handle.ProviderHandle;
import handle.ServiceHandle;
import javafx.util.Pair;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import util.Alert;

/**
 *
 * @author Admin
 */
@Controller
@RequestMapping("/Ebms/Service")
public class ServiceController {

    @RequestMapping
    public String service(ModelMap mm) throws Exception {
        mm.put("LIST_ITEM", new ServiceHandle().findAllAvailable());
        return "Admin.Service";
    }

    @RequestMapping(value = "/Content", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView serviceContent(ModelMap mm) throws Exception {
        mm.put("LIST_ITEM", new ServiceHandle().findAllAvailable());
        return new ModelAndView("Admin.Service.Content");
    }

    @RequestMapping(value = "/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView viewInsert(ModelMap mm) throws Exception {
        mm.put("LIST_PROVIDER", new ProviderHandle().findAllAvailable());
        return new ModelAndView("Admin.Service.ViewInsert");
    }

    @RequestMapping(value = "/Insert", method = RequestMethod.POST)
    @ResponseBody
    public Pair insert(@RequestBody Service service) {
        try {

            new ServiceHandle().create(service);
            return new Pair(1, Alert.createSuccessAlert("Thêm dịch vụ thành công!"));
        } catch (ConstraintViolationException cve) {
            cve.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Dịch vự đã tồn tại! Thêm thất bại"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Thêm dịch vụ thất bại"));
        }
    }

    @RequestMapping(value = "/ViewEdit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewEdit(@PathVariable(value = "id") Integer id, ModelMap mm) throws Exception {
        mm.put("LIST_PROVIDER", new ProviderHandle().findAllAvailable());
        mm.put("SELECTED_ITEM", new ServiceHandle().findEager(id));
        return new ModelAndView("Admin.Service.ViewEdit");
    }

    @RequestMapping(value = "/Edit", method = RequestMethod.POST)
    @ResponseBody
    public Pair Edit(@RequestBody Service service) {
        try {
            new ServiceHandle().update(service);
            return new Pair(1, Alert.createSuccessAlert("Chỉnh sửa dịch vụ thành công"));
        } catch (ConstraintViolationException cve) {
            cve.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Đường dẫn đã tồn tại! Thêm thất bại"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Chỉnh sửa dịch vụ thất bại"));
        }
    }

    @RequestMapping(value = "/Delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Pair deleteAdminRole(@PathVariable(value = "id") Integer id) {
        try {
            new ServiceHandle().delete(id);
            return new Pair(1, Alert.createSuccessAlert("Xoá dịch vụ thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Xoá dịch vụ thất bại"));
        }
    }
}
