<%-- 
    Document   : roomtype
    Created on : Jan 12, 2020, 11:41:20 AM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="/WEB-INF/tlds/functions" prefix="f" %>
<!--================== PAGE-COVER ===============-->
<div class="living_middle">

    <style type="text/css">
        select, ul { height: 100px; overflow: auto; width: 100px; }
        ul { list-style-type: none; margin: 0; padding: 0; overflow-x: hidden;width: auto; }
        li { margin: 0; padding: 0; }
        label { display: block; color: WindowText; background-color: Window; margin: 0; padding: 0; width: 100%; }
        label:hover { background-color: Highlight; color: HighlightText; }
    </style>
    <div class="container">
        <div class="col-md-3 wow fadeInLeft" data-wow-delay="0.4s">
            <br>
            <a class="feature" href="/info/thongtintaikhoan">
                <ul class="feature" >
                    <li><i class=""></i></li>
                    <li class="feature_right"><h4>Thông tin tài khoản</h4>
                        <p></p>
                    </li>
                    <div class="clearfix"></div>
                </ul>
            </a>
            <a class="feature" href="/info/doimatkhau">
                <ul class="feature"  >
                    <li><i class=""></i></li>
                    <li class="feature_right"><h4>Đổi mật khẩu</h4>
                        <p></p>
                    </li>
                    <div class="clearfix"></div>
                </ul>
            </a>
<%--            <a class="feature" href="/info/eventinstancecare">--%>
<%--                <ul class="feature" >--%>
<%--                    <li><i class=""></i></li>--%>
<%--                    <li class="feature_right"><h4>Quan tâm</h4>--%>
<%--                        <p></p>--%>
<%--                    </li>--%>
<%--                    <div class="clearfix"></div>--%>
<%--                </ul>--%>
<%--            </a>--%>
            <a class="feature" href="/logout">
                <ul class="feature">
                    <li><i class=""></i></li>
                    <li class="feature_right"><h4>Thoát</h4>
                        <p></p>
                    </li>
                    <div class="clearfix"></div>
                </ul>
            </a>
        </div>
        <div class="col-md-9 wow fadeInRight animated" data-wow-delay="0.4s"
             style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
            <div class="educate_grid">
                <div class="col-md-12">
                    <div class="living_box">
                        <div class="img-responsive">
                            <form id="my-form"  action="/info/updateinfo">
                               <div class="form-group">
                                   <label>Họ tên</label>
                                   <input name="name"  value="${CUSTOMER_CURRENT.name}" class="form-control" required>
                               </div>
                                <div class="form-group">
                                    <label>Ngày sinh</label>
                                    <input id="dateOfBirth"
                                           value="${f:customFormatDate('dd/MM/yyyy',CUSTOMER_CURRENT.dateOfBirth)}"
                                           class="form-control datetimepicker"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label>Lĩnh vực quan tâm</label>
                                    <ul>
                                        <c:forEach items="${LIST_FIELD}" var="item">
                                            <c:choose>
                                                <c:when test="${f:isStringContainInteger(CUSTOMER_CURRENT.fieldCaringIDs,item.id)}">
                                                    <li class="custom-control custom-checkbox">
                                                        <label for="field${item.id}" class="form-check-label">
                                                            <input type="checkbox" checked value="${item.id}" name="fieldCaringIDs" id="field${item.id}" class="custom-control-input">
                                                            <span style="font-weight: normal;" class="custom-control-label" for="field${item.id}">${item.name}</span>
                                                        </label>
                                                    </li>
                                                </c:when>
                                                <c:otherwise>
                                                    <li class="custom-control custom-checkbox">
                                                        <label for="field${item.id}" class="form-check-label">
                                                            <input type="checkbox" value="${item.id}" name="fieldCaringIDs" id="field${item.id}" class="custom-control-input">
                                                            <span style="font-weight: normal;" class="custom-control-label" for="field${item.id}">${item.name}</span>
                                                        </label>
                                                    </li>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </ul>
                                </div>

                               <div class="form-group">
                                   <label>Ngày tạo</label>
                                   <label class="form-control">
                                       ${f:customFormatDate('dd/MM/yyyy',CUSTOMER_CURRENT.createdDate)}
                                   </label>
                               </div>

                            <div class="living_desc desc1">
                                <h3></h3>
                                <p><a href=""></a></p>
                                <button class="btn btn-success" type="submit">Xác nhận</button>
                                <div class="clearfix"></div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#my-form').validate({
            submitHandler: function () {
                var data = $('#my-form').serializeObject();
                var url = $('#my-form').attr('action');
                if ($('#dateOfBirth').val()) {
                    data.dateOfBirth = new Date(moment($('#dateOfBirth').val(), 'DD/MM/YYYY').format('YYYY/MM/DD'));
                }
                var fields = [];
                $.each($("input[name='fieldCaringIDs']:checked"), function () {
                    fields.push($(this).val());
                });
                data.fieldCaringIDs = fields.join(",");
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    success: function (result) {
                        openAlert(result.value);
                        if(result.key==1){
                            window.location.href = "/logout";
                        }
                    },
                    error: function () {
                        errorAlert();
                    }
                });
            }
        });
        var firstOpen;
        $('.datetimepicker').datetimepicker({
            viewMode: 'years',
            format: 'DD/MM/YYYY',
            icons: {
                previous: 'fas fa-angle-left',
                next: 'fas fa-angle-right',
            }
        }).on("dp.show", function () {
            if (typeof firstOpen === 'undefined') {
                $(this).data('DateTimePicker').date("01/01/1997");
                firstOpen = false;
            }
        });
    });
</script>