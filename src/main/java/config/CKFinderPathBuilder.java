/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import com.ckfinder.connector.configuration.DefaultPathBuilder;
import entity.Admin;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Admin
 */
public class CKFinderPathBuilder extends DefaultPathBuilder {

    @Override
    public String getBaseDir(HttpServletRequest request) {
        try {
            Admin admin = (Admin) request.getSession().getAttribute("ADMIN");
            if (admin != null) {
                return "C:/Upload/Ebms/" + admin.getUsername();
            }
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public String getBaseUrl(HttpServletRequest request) {
        try {
            Admin admin = (Admin) request.getSession().getAttribute("ADMIN");
            if (admin != null) {
                return "/Upload/" + admin.getUsername() + "/";
            }
        } catch (Exception e) {
        }
        return null;
    }
}
