/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handle;

import entity.Ticket;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

/**
 *
 * @author 1997n
 */
public class TicketHandle extends AbstractHandle {

    public TicketHandle() {
        super(Ticket.class);
    }

    public List findAllTicketAvaiable(Integer eventInstanceID, boolean isGuests, Integer maxResult) {
        List rs = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Ticket.class);
                cr.createAlias("eventInstanceID", "eventInstanceID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("eventInstanceID", FetchMode.JOIN);
                if (eventInstanceID != null) {
                    cr.add(Restrictions.eq("eventInstanceID.id", eventInstanceID));
                }
                cr.add(Restrictions.eq("isGuests", isGuests));
                cr.add(Restrictions.eq("isSold", false));
                if (maxResult != null) {
                    cr.setMaxResults(maxResult);
                }
                rs = (List<Ticket>) cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }

}
