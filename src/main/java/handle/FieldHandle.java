/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handle;

import entity.Field;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 *
 * @author Customer
 */
public class FieldHandle extends AbstractHandle {
    public FieldHandle() {
        super(Field.class);
    }
    public String getNameField(List<Integer> lst){
        String name="";
        Session session = null;
        List<Field> result;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Field.class);
                cr.add(Restrictions.in("id", lst));
                result =  cr.list();
                for(Field field : result){
                    name +=field.getName()+" -";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return name.substring(0,name.length()-1);
    }
}
