package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Customer")
public class Customer implements Serializable {

    private Integer id;
    private String name;
    private Date dateOfBirth;
    private String username;
    private String password;
    private String fieldCaringIDs;
    private String eventCaringIDs;
    private String eventInstanceCaringIDs;
    private Boolean isDeleted;
    private Date createdDate;

    @Id
    @Column(name = "ID", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Column(name = "FieldCaringIDs")
    public String getFieldCaringIDs() {
        return fieldCaringIDs;
    }

    public void setFieldCaringIDs(String fieldCaringIDs) {
        this.fieldCaringIDs = fieldCaringIDs;
    }
    @Column(name = "EventCaringIDs")
    public String getEventCaringIDs() {
        return eventCaringIDs;
    }

    public void setEventCaringIDs(String eventCaringIDs) {
        this.eventCaringIDs = eventCaringIDs;
    }

    @Column(name = "EventCaringIDs",insertable = false,updatable = false)
    public String getEventInstanceCaringIDs() {
        return eventInstanceCaringIDs;
    }

    public void setEventInstanceCaringIDs(String eventInstanceCaringIDs) {
        this.eventInstanceCaringIDs = eventInstanceCaringIDs;
    }
    @Column(name = "DateOfBirth")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Column(name = "Username", updatable = false)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "Password", updatable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "IsDeleted", insertable = false, updatable = false)
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedDate", insertable = false, updatable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
