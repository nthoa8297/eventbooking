package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "Service")
public class Service implements Serializable {

    private Integer id;
    private String name;
    private BigDecimal price;
    private String description;
    private Date createdDate;
    private Boolean isDeleted;
    private Provider providerID;


    public Service() {
    }

    public Service(Integer id) {
        this.id = id;
    }

    @Id
    @Column(name = "ID", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "Price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "IsDeleted", insertable = false, updatable = false)
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ProviderID")
    public Provider getProviderID() {
        return providerID;
    }
    public void setProviderID(Provider providerID) {
        this.providerID = providerID;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedDate", insertable = false, updatable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
