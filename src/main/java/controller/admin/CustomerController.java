/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import controller.*;
import entity.Customer;
import entity.Field;
import handle.CustomerHandle;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import handle.EventHandle;
import handle.FieldHandle;
import javafx.util.Pair;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import util.Alert;
import util.CustomFunction;

/**
 *
 * @author Admin
 */
@Controller
@RequestMapping("/Ebms/Customer")
public class CustomerController {

    @RequestMapping
    public String customer(ModelMap mm) throws Exception {
        List<Customer> lst = (List<Customer>)new CustomerHandle().findAllAvailable();
        for(Customer dto : lst){
            if( !StringUtils.isEmpty(dto.getFieldCaringIDs())
                    && dto.getFieldCaringIDs().length()>0){
                String[] arrFieldID = dto.getFieldCaringIDs().split(",");
                List<String> list = Arrays.asList(arrFieldID);
                List<Integer> integerList = list.stream()
                        .map(s -> Integer.parseInt(s))
                        .collect(Collectors.toList());
                dto.setFieldCaringIDs(new FieldHandle().getNameField(integerList));
            }
//-------------------------------//
            if( !StringUtils.isEmpty(dto.getEventCaringIDs())
                    && dto.getEventCaringIDs().length()>0){
                String[] arrEventID = dto.getEventCaringIDs().split(",");
                List<String> list = Arrays.asList(arrEventID);
                List<Integer> integerList = list.stream()
                        .map(s -> Integer.parseInt(s))
                        .collect(Collectors.toList());
                dto.setEventCaringIDs(new EventHandle().getNameEvent(integerList));
            }
        }
        mm.put("LIST_ITEM", lst);
        return "Admin.Customer";
    }

    @RequestMapping(value = "/Content", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView customerContent(ModelMap mm) throws Exception {
        List<Customer> lst = (List<Customer>)new CustomerHandle().findAllAvailable();
        for(Customer dto : lst){
            if( !StringUtils.isEmpty(dto.getFieldCaringIDs())
                    && dto.getFieldCaringIDs().length()>0){
                String[] arrFieldID = dto.getFieldCaringIDs().split(",");
                List<String> list = Arrays.asList(arrFieldID);
                List<Integer> integerList = list.stream()
                        .map(s -> Integer.parseInt(s))
                        .collect(Collectors.toList());
                dto.setFieldCaringIDs(new FieldHandle().getNameField(integerList));
            }
//-------------------------------//
            if( !StringUtils.isEmpty(dto.getEventCaringIDs())
                    && dto.getEventCaringIDs().length()>0){
                String[] arrEventID = dto.getEventCaringIDs().split(",");
                List<String> list = Arrays.asList(arrEventID);
                List<Integer> integerList = list.stream()
                        .map(s -> Integer.parseInt(s))
                        .collect(Collectors.toList());
                dto.setEventCaringIDs(new EventHandle().getNameEvent(integerList));
            }
        }
        mm.put("LIST_ITEM", lst);
        return new ModelAndView("Admin.Customer.Content");
    }

    @RequestMapping(value = "/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView viewInsert(ModelMap mm) throws Exception {
        mm.put("LIST_FIELD", new FieldHandle().findAll());
        mm.put("LIST_EVENT", new EventHandle().findAllAvailable());
        return new ModelAndView("Admin.Customer.ViewInsert");
    }

    @RequestMapping(value = "/Insert", method = RequestMethod.POST)
    @ResponseBody
    public Pair insert(@RequestBody Map mapCustomer) {
        try {
            Customer customer = (Customer) new CustomerHandle().getObjectFromMap(mapCustomer);
            customer.setPassword(CustomFunction.md5(customer.getPassword()));
            new CustomerHandle().create(customer);
            return new Pair(1, Alert.createSuccessAlert("Thêm khách hàng thành công!"));
        } catch (ConstraintViolationException cve) {
            cve.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Tài khoản đã tồn tại!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Thêm khách hàng thất bại"));
        }
    }

    @RequestMapping(value = "/ViewEdit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewEdit(@PathVariable(value = "id") Integer id, ModelMap mm) throws Exception {
        mm.put("LIST_FIELD", new FieldHandle().findAll());
        mm.put("LIST_EVENT", new EventHandle().findAllAvailable());
        mm.put("SELECTED_ITEM", new CustomerHandle().findById(id));
        return new ModelAndView("Admin.Customer.ViewEdit");
    }

    @RequestMapping(value = "/Edit", method = RequestMethod.POST)
    @ResponseBody
    public Pair Edit(@RequestBody Map mapCustomer) {
        try {
            Customer customer = (Customer) new CustomerHandle().getObjectFromMap(mapCustomer);
            new CustomerHandle().update(customer);
            return new Pair(1, Alert.createSuccessAlert("Sửa thông tin khách hàng thành công!"));
        } catch (ConstraintViolationException cve) {
            cve.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Tài khoản đã tồn tại!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Sửa thông tin khách hàng thất bại"));
        }
    }

    @RequestMapping(value = "/Delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Pair deleteAdminRole(@PathVariable(value = "id") Integer id) {
        try {
            new CustomerHandle().delete(id);
            return new Pair(1, Alert.createSuccessAlert("Xoá khách hàng thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Xoá khách hàng thất bại"));
        }
    }
}
