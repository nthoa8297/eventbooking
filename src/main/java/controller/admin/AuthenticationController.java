package controller.admin;

import entity.Admin;
import entity.EventInstance;
import handle.AdminHandle;
import handle.EventInstanceHandle;
import javafx.util.Pair;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import util.Alert;
import util.CustomFunction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping("/Ebms")
public class AuthenticationController {

    @RequestMapping(value = "/Login", method = RequestMethod.GET)
    public String login(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Admin admin = (Admin) request.getSession().getAttribute("ADMIN");
        if (admin != null) {
            response.sendRedirect("/Ebms");
        }

        return "Admin.Login";
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String adminPage(ModelMap mm) {
        List<EventInstance> lst =  new EventInstanceHandle().getTOP5();
        mm.put("LIST_ITEM",lst);
        return "Ebms";
    }

    @RequestMapping(value = "/Login", method = RequestMethod.POST)
    @ResponseBody
    public Pair login(@RequestBody Admin adm, HttpSession session) {
        try {
            Admin admin = new AdminHandle().checkLogin(adm.getUsername(), CustomFunction.md5(adm.getPassword()));
            if (admin == null) {
                return new Pair(0, Alert.createErrorAlert("Sai tên tài khoản hoặc mật khẩu"));
            } else {
                session.setAttribute("ADMIN", admin);
                return new Pair(1, Alert.createRedirectPage("/Ebms"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Pair(
                0, Alert.createErrorAlert("Đã xảy ra lỗi! Vui lòng thử lại!"));
    }

    @RequestMapping(value = "/Logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IllegalStateException {
        request.getSession().invalidate();
        try {
            response.sendRedirect("/Ebms/Login");
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(AuthenticationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @RequestMapping(value = "/ChangePassword", method = RequestMethod.POST)
    @ResponseBody
    public Pair changePassword(@RequestBody Map map, HttpServletRequest request, HttpSession session) {
        try {
            Admin admin = (Admin) session.getAttribute("ADMIN");
            Admin result = new AdminHandle().checkLogin(admin.getUsername(), CustomFunction.md5((String) map.get("oldPassword")));
            if (result == null) {
                return new Pair(0, Alert.createErrorAlert("Sai mật khẩu!"));
            } else {
                result.setPassword(CustomFunction.md5((String) map.get("newPassword")));
                new AdminHandle().update(result);
                request.getSession().invalidate();
                return new Pair(1, Alert.createIntervalRedirect("Thay đổi mật khẩu thành công!",
                        "Đăng nhập lại sau ", "/Ebms", 3000));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Pair(0, Alert.createErrorAlert("Đã xảy ra lỗi!"));
    }

    @RequestMapping(value = "/CheckPassword", method = RequestMethod.POST)
    @ResponseBody
    public boolean checkPassword(@RequestParam(value = "oldPassword") String oldPassword, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("ADMIN");
        Admin result = new AdminHandle().checkLogin(admin.getUsername(), CustomFunction.md5(oldPassword));
        if (result != null) {
            return true;
        } else {
            return false;
        }
    }
}
