<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">          
            <form id="my-form" novalidate action="/Ebms/Admin/Edit">
                <input type="hidden" value="${SELECTED_ITEM.id}" name="id"/>
                <div class="modal-header">
                    <h4 class="modal-title">Sửa thông tin</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Tên nhân viên</label>
                            <input value="${SELECTED_ITEM.name}" name="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Chức vụ</label>
                            <select class="form-control" data-type="int" data-json="id" name="adminRoleID">
                                <option value="">-Chọn chức vụ-</option>
                                <c:forEach items="${AVAILABLE_ADMINROLES}" var="item">
                                    <option ${SELECTED_ITEM.adminRoleID.id==item.id?'selected':''} value="${item.id}">${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Huỷ</button>
                    <button type="submit" id="btn-submit" class="btn btn-primary">Lưu</button>
                </div>      
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <script>
        $(document).ready(function () {
            $('#my-form').validate({
                submitHandler: function () {
                    var data = $('#my-form').serializeObject();
                    var url = $('#my-form').attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function (result) {
                            openAlert(result.value);
                            if (result.key) {
                                $('#myModal').modal('hide');
                                reloadAjaxContent();
                            }
                        },
                        error: function () {
                            errorAlert();
                        }
                    });
                }
            });
        });
    </script>
</div>