package handle;

import entity.ServiceMapping;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class ServiceMappingHandle extends AbstractHandle {

    public ServiceMappingHandle() {
        super(ServiceMapping.class);
    }
    public List findByEventInstanceID(Integer eventInstanceID) throws Exception {
        Session session = null;
        List result = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(ServiceMapping.class);
                cr.createAlias("serviceID", "serviceID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("serviceID", FetchMode.JOIN).setFetchMode("serviceID", FetchMode.JOIN);
                cr.add(Restrictions.eq("eventInstanceID", eventInstanceID));
                result =  cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
