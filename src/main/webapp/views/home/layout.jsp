<%-- 
    Document   : layout
    Created on : Jan 11, 2020, 11:34:52 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri ="/WEB-INF/tlds/functions" prefix="f"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" href="/resources/images/home/favicon.png" type="image/x-icon">
        <title>DUHOOT</title>

        <!-- Google Fonts -->	
        <link href="/resources/shared/css/datepicker.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/noty/noty.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/noty/nest.css" rel="stylesheet" type="text/css"/>
        <%--   template     --%>

        <link href="/resources/shared/css/bootstrap1.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Custom Theme files -->
        <link href="/resources/shared/css/style1.css" rel='stylesheet' type='text/css' />
        <script type="application/x-javascript">
            addEventListener("load", function() {
                setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <script src="/resources/shared/js-home/jquery.min.js" type="text/javascript"></script>
        <link href="/resources/shared/datetime-picker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/resources/shared/js/login.js"></script>
        <link href="/resources/shared/css/style1.css" rel='stylesheet' type='text/css'/>
        <script src="/resources/shared/js/jquery.easydropdown.js"></script>
        <script src="/resources/shared/js/wow.min.js"></script>
        <script src="/resources/shared/js/wow.min.js"></script>
        <link href="/resources/shared/css/animate.css" rel='stylesheet' type='text/css'/>
        <script>
            new WOW().init();
        </script>
        <link href="/resources/shared/data-table/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="/resources/shared/js/jquery.validate.js" type="text/javascript"></script>
    </head>
    <body>
    <tiles:insertAttribute name="Header" />
<%--       end header--%>
    <div id="ajax-content">
    <tiles:insertAttribute name="Content" />
    </div>
    <tiles:insertAttribute name="Footer" />


    <!-- Page Scripts Starts -->
    <script src="/resources/shared/js-home/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/resources/shared/datetime-picker/moment.min.js" type="text/javascript"></script>
    <script src="/resources/shared/noty/noty.min.js" type="text/javascript"></script>
    <script src="/resources/custom/customJS.js" type="text/javascript"></script>
    <script src="/resources/shared/js/jquery.form.js" type="text/javascript"></script>
    <script src="/resources/shared/js/serialize.js" type="text/javascript"></script>
    <script src="/resources/shared/datetime-picker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="/resources/shared/js-home/jquery.magnific-popup.min.js" type="text/javascript"></script>
    <script src="/resources/shared/js-home/bootstrap.min.js" type="text/javascript"></script>
    <script src="/resources/shared/js-home/jquery.flexslider.js" type="text/javascript"></script>
    <script src="/resources/shared/js-home/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/resources/shared/js-home/owl.carousel.min.js" type="text/javascript"></script>
    <script src="/resources/shared/js-home/custom-navigation.js" type="text/javascript"></script>
    <script src="/resources/shared/js-home/custom-flex.js" type="text/javascript"></script>
    <script src="/resources/shared/js-home/custom-owl.js" type="text/javascript"></script>
    <script src="/resources/shared/fontawesome/fontawesome.min.js" type="text/javascript"></script>
    <script src="/resources/shared/js-home/custom-date-picker.js?v=1212" type="text/javascript"></script>
    <script src="/resources/shared/js/jquery.qrcode.min.js"></script>
    <script src="/resources/shared/data-table/jquery.dataTables.js" type="text/javascript"></script>
    <script src="/resources/shared/data-table/dataTables.bootstrap.js" type="text/javascript"></script>

    <!-- Page Scripts Ends -->
    <script>

        jQuery.extend(jQuery.validator.messages, {
            required: "Thông tin này là bắt buộc!",
            remote: "Thông tin nhập vào không chính xác",
            equalTo: "Dữ liệu không trùng khớp",
            maxlength: jQuery.validator.format("Nhập tối đa {0} ký tự"),
            minlength: jQuery.validator.format("Nhập tối thiểu {0} ký tự"),
            max: jQuery.validator.format("Nhập giá trị tối đa lớn hơn hoặc bằng{0}"),
            min: jQuery.validator.format("Nhập giá trị tối thiểu lớn hơn hoặc bằng {0}")
        });
    </script>
    <script>
        $(function () {
            var path = window.location.pathname;
            path = path.substring(1, path.length);
            var linkLogin = $('#link-login').attr('href');
            $('#link-login').attr('href', linkLogin + '?r=/' + path);
            $('#link-login-2').attr('href', linkLogin + '?r=/' + path);
        });
    </script>
    </body>
</html>
