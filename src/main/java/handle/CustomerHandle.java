/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handle;

import entity.Customer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 *
 * @author Customer
 */
public class CustomerHandle extends AbstractHandle {

    public CustomerHandle() {
        super(Customer.class);
    }

    public Customer checkLogin(String username, String password) {
        Customer customer = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Customer.class);
                cr.add(Restrictions.eq("username", username));
                cr.add(Restrictions.eq("password", password));
                cr.add(Restrictions.eq("isDeleted", false));
                customer = (Customer) cr.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return customer;
    }
    public Integer changePassword(Integer id, String password) throws Exception {
        Session session = null;
        Integer result = 0;
        Transaction tran = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            tran = session.beginTransaction();
            if (session != null) {
                session.beginTransaction();
                result = session.createSQLQuery("update Customer set Password=:password where Id=:id")
                        .setParameter("password", password)
                        .setParameter("id", id)
                        .executeUpdate();
                HibernateConfiguration.getInstance().commitTransaction(tran);
            }
        } catch (Exception e) {
            HibernateConfiguration.getInstance().rollbackTransaction(tran);
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

}
