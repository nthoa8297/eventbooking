package controller.admin;

import entity.*;
import handle.*;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.*;

import javafx.util.Pair;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import util.Alert;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/Ebms/EventInstance")
public class EventInstanceController {

    @RequestMapping("")
    public String eventInstance(
            @RequestParam(name = "currentPage", required = false, defaultValue = "1") Integer currentPage,
            @RequestParam(name = "status", required = false) Integer status,
            ModelMap mm) throws Exception {
        Map pager = new HashMap();
        pager.put("currentPage", currentPage);
        pager.put("status", status);
        List<EventInstance> listItem = new EventInstanceHandle().paging(pager);
        listItem.forEach(item ->{
            try {
                item.setServiceMappings((List<ServiceMapping>) new ServiceMappingHandle().findByEventInstanceID(item.getId()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        for (EventInstance item : listItem) {
            item.setNormalTicket(new TicketHandle().findAllTicketAvaiable(item.getId(), false, null).size());
            item.setVipTicket(new TicketHandle().findAllTicketAvaiable(item.getId(), true, null).size());

            if (!StringUtils.isEmpty(item.getEventID().getFieldIDs())
                    && item.getEventID().getFieldIDs().length() > 0) {
                String[] arrFieldID = item.getEventID().getFieldIDs().split(",");

                String regex ="\\d+";
                List<String> listString = Arrays.asList(arrFieldID);
                if(listString.size()==1){
                    item.getEventID().setFieldIDs(listString.get(0));
                }else{
                    List<String> listFillter = new ArrayList<>();
                    listString.stream()
                            .filter(string -> string.matches(regex))
                            .collect(Collectors.toList())
                            .forEach(x->{
                                listFillter.add(x);
                            });
                    List<Integer> integerList = listFillter.stream()
                            .map(s -> Integer.parseInt(s))
                            .collect(Collectors.toList());
                    item.getEventID().setFieldIDs(new FieldHandle().getNameField(integerList));
                }
            }
        }
        mm.put("LIST_ITEM", listItem);
        mm.put("PAGER", pager);
        return "Admin.EventInstance";
    }


    @RequestMapping(value = "/Content", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView eventInstanceContent(
            @RequestParam(name = "currentPage", required = false, defaultValue = "1") Integer currentPage,
            @RequestParam(name = "status", required = false) Integer status,
            ModelMap mm) throws Exception {
        Map pager = new HashMap();
        pager.put("currentPage", currentPage);
        pager.put("status", status);
        List<EventInstance> listItem = new EventInstanceHandle().paging(pager);
        listItem.forEach(item ->{
            try {
                item.setServiceMappings((List<ServiceMapping>) new ServiceMappingHandle().findByEventInstanceID(item.getId()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        for (EventInstance item : listItem) {
            item.setNormalTicket(new TicketHandle().findAllTicketAvaiable(item.getId(), false, null).size());
            item.setVipTicket(new TicketHandle().findAllTicketAvaiable(item.getId(), true, null).size());

            if (!StringUtils.isEmpty(item.getEventID().getFieldIDs())
                    && item.getEventID().getFieldIDs().length() > 0) {
                String[] arrFieldID = item.getEventID().getFieldIDs().split(",");

                String regex ="\\d+";
                List<String> listString = Arrays.asList(arrFieldID);
                if(listString.size()==1){
                    item.getEventID().setFieldIDs(listString.get(0));
                }else{
                    List<String> listFillter = new ArrayList<>();
                    listString.stream()
                            .filter(string -> string.matches(regex))
                            .collect(Collectors.toList())
                            .forEach(x->{
                                listFillter.add(x);
                            });
                    List<Integer> integerList = listFillter.stream()
                            .map(s -> Integer.parseInt(s))
                            .collect(Collectors.toList());
                    item.getEventID().setFieldIDs(new FieldHandle().getNameField(integerList));
                }
            }
        }
        mm.put("LIST_ITEM", listItem);
        mm.put("PAGER", pager);
        return new ModelAndView("Admin.EventInstance.Content");
    }

    @RequestMapping(value = "/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView viewInsert(ModelMap mm) throws Exception {
        mm.put("LIST_ADDRESS", new AddressHandle().findAllAvailable());
        mm.put("LIST_EVENT", new EventHandle().findAllAvailable());
        mm.put("LIST_SERVICE", new ServiceHandle().findAllAvailable());
        return new ModelAndView("Admin.EventInstance.ViewInsert");
    }

    @RequestMapping(value = "/Insert", method = RequestMethod.POST)
    @ResponseBody
    public Pair insert(@RequestBody Map mapEventInstance) {
        try {
            EventInstance eventInstance = (EventInstance) new EventInstanceHandle().getObjectFromMap(mapEventInstance);
            new EventInstanceHandle().save(eventInstance);
            return new Pair(1, Alert.createSuccessAlert("Thêm sự kiện thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Thêm sự kiện thất bại"));
        }
    }

    @RequestMapping(value = "/ViewEdit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView viewEdit(
            @PathVariable(value = "id") int id, ModelMap mm) throws Exception {
        EventInstance selectedItem = new EventInstanceHandle().findEager(id);
        selectedItem.setServiceMappings(new ServiceMappingHandle().findByEventInstanceID(id));
        selectedItem.setVipTicket(new TicketHandle().findAllTicketAvaiable(id, true, null).size());
        List<Ticket> normalTickets = new TicketHandle().findAllTicketAvaiable(id, false, null);
        selectedItem.setNormalTicket(normalTickets.size());
        selectedItem.setTicketPrice(normalTickets.get(0).getPrice());

        mm.put("SELECTED_ITEM", selectedItem);
        mm.put("LIST_ADDRESS", new AddressHandle().findAllAvailable());
        mm.put("LIST_EVENT", new EventHandle().findAllAvailable());
        mm.put("LIST_SERVICE", new ServiceHandle().findAllAvailable());
        return new ModelAndView("Admin.EventInstance.ViewEdit");
    }

    @RequestMapping(value = "/Edit", method = RequestMethod.POST)
    @ResponseBody
    public Pair edit(@RequestBody Map mapEventInstance) {
        try {
            EventInstance eventInstance = (EventInstance) new EventInstanceHandle().getObjectFromMap(mapEventInstance);
            new EventInstanceHandle().save(eventInstance);
            return new Pair(1, Alert.createSuccessAlert("Điều chỉnh thông tin sự kiện thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Điều chỉnh thông tin sự kiện thất bại"));
        }
    }

    @RequestMapping(value = "/ChangeStatus/{id}")
    @ResponseBody
    public Pair edit(@PathVariable(value = "id") Integer id,
            @RequestParam(name = "status") Integer status) {
        try {
            new EventInstanceHandle().updateStatus(id, "status", status);
            return new Pair(1, Alert.createSuccessAlert("Cập nhật trạng thái thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Cập nhật trạng thái thất bại"));
        }
    }

    @RequestMapping(value = "/SellTicket/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView sellTicket(
            @PathVariable(value = "id") int id, ModelMap mm) throws Exception {
        EventInstance selectedItem = new EventInstanceHandle().findEager(id);
        selectedItem.setVipTicket(new TicketHandle().findAllTicketAvaiable(id, true, null).size());
        List<Ticket> normalTickets = new TicketHandle().findAllTicketAvaiable(id, false, null);
        selectedItem.setNormalTicket(normalTickets.size());
        selectedItem.setTicketPrice(normalTickets.get(0).getPrice());
        mm.put("SELECTED_ITEM", selectedItem);
        return new ModelAndView("Admin.EventInstance.ViewSellTicket");
    }

    @RequestMapping(value = "/SellTicket/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Pair sellTicket(@PathVariable(value = "id") int id, @RequestBody Map mapBill,HttpSession session) {
        
            Admin admin = (Admin) session.getAttribute("ADMIN");
        try {
            Bill bill = (Bill) new BillHandle().getObjectFromMap(mapBill);
            List<BillDetail> billDetails = new ArrayList<>();
            if (bill.getVipTicket() > 0) {
                List<Ticket> tickets = new TicketHandle().findAllTicketAvaiable(id, true, bill.getVipTicket());
                for (Ticket item : tickets) {
                    billDetails.add(new BillDetail(item));
                }
            }
            if (bill.getNormalTicket() > 0) {
                List<Ticket> tickets = new TicketHandle().findAllTicketAvaiable(id, false, bill.getNormalTicket());
                for (Ticket item : tickets) {
                    billDetails.add(new BillDetail(item));
                }
            }
            bill.setAdminID(admin);
            bill.setBillDetails(billDetails);
            new BillHandle().save(bill);
            return new Pair(1, Alert.createSuccessAlert("Điều chỉnh thông tin sự kiện thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Điều chỉnh thông tin sự kiện thất bại"));
        }
    }
}
