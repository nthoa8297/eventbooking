<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Ebms | Log in</title>
        <!-- Favicon-->
<%--        <link href="/resources/shared/css/all.min.css" rel="stylesheet" type="text/css"/>--%>
        <link href="/resources/shared/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/icheck-bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/adminlte.min.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/fontawesome/fontawesome.min.css" rel="stylesheet" type="text/css"/>



        <!--Noty Css-->
        <link href="/resources/shared/noty/noty.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/shared/noty/nest.css" rel="stylesheet" type="text/css"/>

        <script src="/resources/shared/js/jquery.min.js" type="text/javascript"></script>

    </head>

    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="../../index2.html"><b>Admin</b>LTE</a>
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Sign in to start your session</p>

                    <form id="my-form" novalidate action="/Ebms/Login">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" id="username" name="username" value="manager" placeholder="Tài khoản" required autofocus>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control" id="password" name="password" value="123" placeholder="Mật khẩu" required>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="remember">
                                    <label for="remember">
                                        Remember Me
                                    </label>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>

                    <div class="social-auth-links text-center mb-3">
                        <p>- OR -</p>
                        <a href="#" class="btn btn-block btn-primary">
                            <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
                        </a>
                        <a href="#" class="btn btn-block btn-danger">
                            <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
                        </a>
                    </div>
                    <!-- /.social-auth-links -->

                    <p class="mb-1">
                        <a href="forgot-password.html">I forgot my password</a>
                    </p>
                    <p class="mb-0">
                        <a href="register.html" class="text-center">Register a new membership</a>
                    </p>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <script src="/resources/shared/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/adminlte.min.js" type="text/javascript"></script>
        <script src="/resources/shared/js/serialize.js" type="text/javascript"></script>
        <script src="/resources/shared/fontawesome/fontawesome.min.js" type="text/javascript"></script>
        <!-- Validation Plugin Js -->
        <script src="/resources/shared/js/jquery.validate.js" type="text/javascript"></script>
        <!-- Custom Js -->
        <script src="/resources/shared/noty/noty.min.js" type="text/javascript"></script>
        <script src="/resources/custom/customJS.js" type="text/javascript"></script>
        <script>
            $('#my-form').validate({
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    username: {
                        required: 'Hãy nhập tài khoản'
                    },
                    password: {
                        required: 'Hãy nhập mật khẩu'
                    }
                },
                submitHandler: function () {
                    var data = $('#my-form').serializeObject();
                    $.ajax({
                        url: '/Ebms/Login',
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function (result) {
                            openAlert(result.value);
                        },
                        error: function () {
                            errorAlert();
                        }
                    });
                }
            });
        </script>
    </body>

</html>
