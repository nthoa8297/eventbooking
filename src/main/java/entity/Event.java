package entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "Event")
public class Event implements Serializable {

    private Integer id;
    private String fieldIDs;
    private String seoContent;
    private String seoTitle;
    private String name;
    private boolean isDeleted;
    private String nameAscii;
    private String urlAvatar;

    @Id
    @Column(name = "ID", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "FieldIDs")
    public String getFieldIDs() {
        return fieldIDs;
    }

    public void setFieldIDs(String fieldIDs) {
        this.fieldIDs = fieldIDs;
    }
    @Column(name = "SeoContent")
    public String getSeoContent() {
        return seoContent;
    }

    public void setSeoContent(String seoContent) {
        this.seoContent = seoContent;
    }
    @Column(name = "SeoTitle")
    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }
    @Column(name = "IsDeleted", insertable = false, updatable = false)
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
    @Column(name = "NameAscii")
    public String getNameAscii() {
        return nameAscii;
    }

    public void setNameAscii(String nameAscii) {
        this.nameAscii = nameAscii;
    }

    @Column(name = "UrlAvatar")
    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }



}
