/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handle;

import entity.Event;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 *
 * @author Customer
 */
public class EventHandle extends AbstractHandle {

    public EventHandle() {
        super(Event.class);
    }
    public String getNameEvent(List<Integer> lst){
        String name="";
        Session session = null;
        List<Event> result;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Event.class);
                cr.add(Restrictions.eq("isDeleted", false));
                cr.add(Restrictions.in("id", lst));

                result =  cr.list();
                if(result.size()>0){
                    for(Event event : result){
                        name +=event.getName()+" -";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return name.substring(0,name.length()-1);
    }
    public boolean checkNameAscii(String nameAscii){
        Session session=null;
        Event event = null;
        boolean check = false;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Event.class);
                cr.add(Restrictions.eq("nameAscii", nameAscii));
                event = (Event) cr.uniqueResult();
                if(event == null){
                    check = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return check;
    }
    public Event findByNameAscii(String nameAscii) {
        Event rs = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Event.class);
                cr.add(Restrictions.eq("nameAscii", nameAscii));
                rs = (Event) cr.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }
}
