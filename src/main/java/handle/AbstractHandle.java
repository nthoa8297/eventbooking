package handle;

import com.fasterxml.jackson.databind.ObjectMapper;
import entity.EventInstance;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public abstract class AbstractHandle<E> implements Serializable {

    public final Class<E> entityClass;

    public AbstractHandle(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    public E getObjectFromMap(Map map) {
        E result = null;
        try {
            final ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
            result = mapper.convertValue(map, entityClass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public E create(E entity) throws Exception {
        Session ss = null;
        Transaction tran = null;
        try {
            ss = HibernateConfiguration.getInstance().openSession();;
            tran = ss.beginTransaction();
            ss.save(entity);
            HibernateConfiguration.getInstance().commitTransaction(tran);
        } catch (Exception e) {
            HibernateConfiguration.getInstance().rollbackTransaction(tran);
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(ss);
        }
        return entity;
    }

    public E update(E entity) throws Exception {
        Session ss = null;
        Transaction tran = null;
        try {
            ss = HibernateConfiguration.getInstance().openSession();;
            tran = ss.beginTransaction();
            ss.update(entity);
            HibernateConfiguration.getInstance().commitTransaction(tran);
        } catch (Exception e) {
            HibernateConfiguration.getInstance().rollbackTransaction(tran);
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(ss);
        }
        return entity;
    }

    public E findById(Integer id) throws Exception {
        Session ss = null;
        E rs = null;
        try {
            ss = HibernateConfiguration.getInstance().openSession();
            rs = (E) ss.get(entityClass, id);
        } catch (Exception e) {
            ss.getTransaction().rollback();
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(ss);
        }
        return rs;
    }

    public List<E> findAll() throws Exception {
        Session ss = null;
        List result = null;
        try {
            ss = HibernateConfiguration.getInstance().openSession();
            if (ss != null) {
                result = (List<E>) ss.createCriteria(entityClass).list();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(ss);
        }
        return result;
    }

    public Integer delete(Integer id) throws Exception {
        Session session = null;
        Transaction tran = null;
        Integer result = 0;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                tran = session.beginTransaction();
                result = session.createSQLQuery("update " + entityClass.getSimpleName() + " set IsDeleted=1 where id=:id")
                        .setParameter("id", id).executeUpdate();
                if (result == 0) {
                    throw new Exception("/There are no record affected");
                }
                HibernateConfiguration.getInstance().commitTransaction(tran);
            }
        } catch (Exception e) {
            HibernateConfiguration.getInstance().rollbackTransaction(tran);
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Integer updateStatus(Integer id, String fieldName, Integer value) throws Exception {
        Session session = null;
        Transaction tran = null;
        Integer result = 0;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                tran = session.beginTransaction();
                result = session.createSQLQuery("update " + entityClass.getSimpleName() + " set " + fieldName + "=:value where id=:id")
                        .setParameter("id", id)
                        .setParameter("value", value)
                        .executeUpdate();
                if (result == 0) {
                    throw new Exception();
                }
                HibernateConfiguration.getInstance().commitTransaction(tran);
            }
        } catch (Exception e) {
            HibernateConfiguration.getInstance().rollbackTransaction(tran);
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
    public List findAllAvailable() throws Exception {
        Session session = null;
        List result = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(entityClass);
                cr.add(Restrictions.eq("isDeleted", false));
                result = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
