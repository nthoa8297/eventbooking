package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "Ticket")
public class Ticket implements Serializable {

    private Integer id;
    private EventInstance eventInstanceID;
    private BigDecimal price;
    private Boolean isGuests;
    private Boolean isSold;

    @Id
    @Column(name = "ID", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EventInstanceID")
    public EventInstance getEventInstanceID() {
        return eventInstanceID;
    }

    public void setEventInstanceID(EventInstance eventInstanceID) {
        this.eventInstanceID = eventInstanceID;
    }

    @Column(name = "Price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Column(name = "IsGuests")
    public Boolean getIsGuests() {
        return isGuests;
    }

    public void setIsGuests(Boolean isGuests) {
        this.isGuests = isGuests;
    }

    @Column(name = "IsSold")
    public Boolean getIsSold() {
        return isSold;
    }

    public void setIsSold(Boolean isSold) {
        this.isSold = isSold;
    }

}
