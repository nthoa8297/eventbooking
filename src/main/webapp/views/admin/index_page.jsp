<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fas fa-chart-pie mr-1"></i>
                    Top sự kiện hấp dẫn nhất
                </h3>

            </div><!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content p-0">
                    <!-- Morris chart - Sales -->
                    <div class="chart tab-pane active" id="revenue-chart"
                         style="position: relative; height: 300px;">
                        <div id="revenue-chart-canvas" height="300" style="height: 300px;">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width: 1%">STT</th>
                                        <th>Sự kiện</th>
                                        <th>Địa chỉ</th>
                                        <th>Loại sự kiện</th>
                                        <th>Thời gian bắt đầu</th>
                                        <th>Thời gian kết thúc</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:if test="${LIST_ITEM != null}">
                                        <c:forEach items="${LIST_ITEM}" var="item" varStatus="index">
                                            <tr>
                                                <td>${index.index+1}</td>
                                                <td>${item.title}</td>
                                                <td>${item.addressID.name}</td>
                                                <td>${item.eventID.name}</td>
                                                <td>${item.startDate}</td>
                                                <td>${item.endDate}</td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
                        <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>                         
                    </div>  
                </div>
            </div><!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- right col -->
</div>
