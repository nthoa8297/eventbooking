<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div id="myModal" class="modal fade" >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form id="my-form" action="/Ebms/EventInstance/SellTicket/${SELECTED_ITEM.id}">
                <div class="modal-header">
                    <h4 class="modal-title">Tạo hoá đơn</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Tên khách hàng</label>
                            <input name="customerName" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Điện thoại khách hàng</label>
                            <input name="customerPhoneNumber" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Ghi chú</label>
                            <input name="note" class="form-control" required>
                        </div>
                        <div>
                            <fieldset>
                                <legend>Chi tiết hoá đơn</legend>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Vé trả phí</label>
                                            <input id="normalTicket" name="normalTicket" placeholder="Còn lại ${SELECTED_ITEM.vipTicket} vé" class="form-control" max="${SELECTED_ITEM.vipTicket}" required value="0">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Vé khách mời</label>
                                            <input name="vipTicket" placeholder="Còn lại ${SELECTED_ITEM.normalTicket} vé" class="form-control" max="${SELECTED_ITEM.normalTicket}" required value="0">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="form-group">
                            <label>Giá vé</label>
                            <input  class="form-control" readonly value="${f:customFormatDecimal('###,### đ',SELECTED_ITEM.ticketPrice)}">
                        </div>
                        <div class="form-group">
                            <label>Thành tiền</label>
                            <input id="amount" class="form-control" readonly value="0">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Huỷ</button>
                    <button type="button" id="btn-submit" class="btn btn-primary">Lưu</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <script>
        $('#normalTicket').on('keyup change', function () {
            var qty = $('#normalTicket').val();
            $('#amount').val(qty *${SELECTED_ITEM.ticketPrice});
        });
        $(document).ready(function () {
            $('#btn-submit').unbind('click');
            $('#btn-submit').click(function () {
                $('#my-form').submit();
            });
            $('#my-form').validate({
                submitHandler: function () {
                    var data = $('#my-form').serializeObject();
                    var url = $('#my-form').attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function (result) {
                            openAlert(result.value);
                            if (result.key) {
                                $('#myModal').modal('hide');
                                reloadAjaxContent();
                            }
                        },
                        error: function () {
                            errorAlert();
                        }
                    });
                }
            });
        });
    </script>
</div>