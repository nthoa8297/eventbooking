<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div id="myModal" class="modal fade">
    <style type="text/css">
        select, ul { height: 100px; overflow: auto; width: 100px; }
        ul { list-style-type: none; margin: 0; padding: 0; overflow-x: hidden;width: auto; }
        li { margin: 0; padding: 0; }
        label { display: block; color: WindowText; background-color: Window; margin: 0; padding: 0; width: 100%; }
        label:hover { background-color: Highlight; color: HighlightText; }
    </style>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">          
            <form id="my-form" novalidate action="/Ebms/Event/Edit">
                <input type="hidden" name="id" value="${SELECTED_ITEM.id}"/>
                <div class="modal-header">
                    <h4 class="modal-title">Sửa loại sự kiện</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"> 
                    <div class="card-body">
                        <div class="form-group">
                            <label>Tên loại sự kiện</label>
                            <input value="${SELECTED_ITEM.name}" name="name" onkeyup="$('#nameAscii[readonly]').val(removeUnicodeURL($(this).val()))" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Ảnh minh hoạ</label>
                            <div class="input-group">
                                <input type="text"  class="form-control" name="urlAvatar" id="urlAvatar"
                                            value="${SELECTED_ITEM.urlAvatar}"/>
                                <div class="input-group-append">
                                    <button type="button" class="input-group-text" id="btn-finder">Chọn file</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Đường dẫn</label>
                            <input id="nameAscii" name="nameAscii" class="form-control" value="${SELECTED_ITEM.nameAscii}" required readonly>
                        </div>
                        <div class="form-group">
                            <label>Lĩnh vực sự kiện</label>
                            <ul>
                            <c:forEach items="${LIST_FIELD}" var="item">
                                <c:choose>
                                    <c:when test="${f:isStringContainInteger(SELECTED_ITEM.fieldIDs,item.id)}">
                                        <li class="custom-control custom-checkbox">
                                            <label for="check${item.id}" class="form-check-label">
                                                <input type="checkbox" value="${item.id}" checked name="fieldIDs" id="check${item.id}" class="custom-control-input">
                                                <label class="custom-control-label" for="check${item.id}">${item.name}</label>
                                            </label>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="custom-control custom-checkbox">
                                            <label for="check${item.id}" class="form-check-label">
                                                <input type="checkbox" value="${item.id}" name="fieldIDs" id="check${item.id}" class="custom-control-input">
                                                <label class="custom-control-label" for="check${item.id}">${item.name}</label>
                                            </label>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                            </ul>
                        </div>
                        <div class="form-group">
                            <label>Nội dung trang</label>
                            <textarea class="form-control ckeditor-content" name="seoContent" id="ckeditor" rows="7">${SELECTED_ITEM.seoContent}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Tiêu đề SEO</label>
                            <input name="seoTitle" value="${SELECTED_ITEM.seoTitle}"  class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Huỷ</button>
                    <button type="button" id="btn-submit" class="btn btn-primary">Lưu</button>
                </div>      
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <script>

        $(document).ready(function () {
            CKEDITOR.replace('ckeditor', {
                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?Type=Images',
                filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?Type=Flash',
                filebrowserUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images',
                language: 'vi',
                height: 300
            });

            CKFinder.config.startupPath = "/images/";
            $(document).on("click", "#btn-finder", function () {
                var finder = new CKFinder({
                    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                    filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?Type=Images',
                    filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?Type=Flash',
                    filebrowserUploadUrl: '/ckfinder/core/connector/java/connector.aspx?command=QuickUpload&type=Files',
                    filebrowserImageUploadUrl: '/ckfinder/core/connector/java/connector.aspx?command=QuickUpload&type=Images',
                    filebrowserFlashUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images'
                });
                finder.selectActionFunction = function (fileUrl) {
                    $('#urlAvatar').val(fileUrl);
                };
                finder.popup();
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#btn-submit').unbind('click');
            $('#btn-submit').click(function () {
                $('#my-form').submit();
            });
            $('#my-form').validate({
                submitHandler: function () {
                    var fields = [];
                    $.each($("input[name='fieldIDs']:checked"), function () {
                        fields.push($(this).val());
                    });
                    var data = $('#my-form').serializeObject();
                    data.fieldIDs = fields.join(",");
                    data.seoContent = CKEDITOR.instances.ckeditor.getData();
                    var url = $('#my-form').attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function (result) {
                            openAlert(result.value);
                            if (result.key) {
                                $('#myModal').modal('hide');
                                reloadAjaxContent();
                            }
                        },
                        error: function () {
                            errorAlert();
                        }
                    });
                }
            });
        });
    </script>
</div>