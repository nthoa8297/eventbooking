package handle;

import entity.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

public class ServiceHandle extends AbstractHandle {

    public ServiceHandle() {
        super(Service.class);
    }
    public Service findEager(Integer id) {
        Service Service = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Service.class);
                cr.createAlias("providerID", "providerID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("providerID", FetchMode.JOIN);
                cr.add(Restrictions.eq("id", id));
                Service = (Service) cr.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return Service;
    }
    public List findAlllHomeAvailable(Map param) throws Exception {
        Session session = null;
        List result = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Service.class);
                cr.add(Restrictions.eq("isDeleted", false));
                if (param != null) {
                    if ((BigDecimal) param.get("minPrice") != null) {
                        cr.add(Restrictions.ge("price", param.get("minPrice")));
                    }
                    if ((BigDecimal) param.get("maxPrice") != null) {
                        cr.add(Restrictions.le("price", param.get("maxPrice")));
                    }
                    if ((Integer) param.get("displayPerPage") != null) {
                        Integer totalResult = cr.list().size();
                        param.put("totalPage", totalResult % 4 > 0 ? Math.round(totalResult / 4 + 1) : totalResult / 4);
                        cr.setFirstResult(((Integer) param.get("currentPage") - 1) * 4);
                        cr.setMaxResults(4);
                    }
                }
                result = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Service findByNameAscii(String nameAscii) throws Exception {
        Session session = null;
        Service result = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Service.class);
                cr.add(Restrictions.eq("isDeleted", false));
                cr.add(Restrictions.eq("nameAscii", nameAscii));
                result = (Service) cr.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

//    public void bookService(BillDetail billDetail, Integer branchId, Customer customerID) {
//        Session session = null;
//        Transaction tran = null;
//        try {
//            session = HibernateConfiguration.getInstance().openSession();
//            if (session != null) {
//                tran = session.beginTransaction();
//                Integer billId = null;
//                billId = (Integer) session.createSQLQuery("SELECT ID FROM Bill WHERE isPaid=0 AND Status=0 AND CustomerID=:customerId")
//                        .setParameter("customerId", customerID.getId())
//                        .uniqueResult();
//
//                if (billId == null) {
//                    Bill bill = new Bill();
////                    bill.setBranchID(new Branch(branchId));
//                    bill.setCustomerInfo(customerID);
//                    session.save(bill);
//                    billId = bill.getId();
//                }
//                billDetail.setBillID(new Bill(billId));
//                billDetail.setIsService(Boolean.TRUE);
//                billDetail.setPrice(billDetail.getServiceID().getPrice());
//                session.save(billDetail);
//                HibernateConfiguration.getInstance().commitTransaction(tran);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            HibernateConfiguration.getInstance().rollbackTransaction(tran);
//        } finally {
//            HibernateConfiguration.getInstance().closeSession(session);
//        }
//    }
}
