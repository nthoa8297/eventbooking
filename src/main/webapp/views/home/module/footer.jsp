<%--
  Created by IntelliJ IDEA.
  User: nthoa
  Date: 4/16/2020
  Time: 11:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="footer">
    <div class="container">
        <div class="footer_top">
            <h3>Subscribe to our newsletter</h3>
            <form>
		<span>
			<i><img src="/resources/images/mail.png" alt="" data-insta_upload_ext_elem="1"></i>
		    <input type="text" value="Enter your email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your email';}">
		    <label class="btn1 btn2 btn-2 btn-2g" style="height: 47px; width: 120px; transition: none 0s ease 0s; cursor: move; position: relative; top: -1px;" data-selected="true" data-label-id="0"> <input name="submit" type="submit" id="submit" value="Subscribe"> </label>
		    <div class="clearfix"> </div>
		</span>
            </form>
        </div>
        <div class="footer_grids">
        </div>
    </div>
</div>