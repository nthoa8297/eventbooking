package controller.admin;

import controller.*;
import entity.Provider;
import handle.ProviderHandle;
import java.util.Map;
import javafx.util.Pair;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import util.Alert;

@Controller
@RequestMapping("/Ebms/Provider")
public class ProviderController {

    @RequestMapping("")
    public String provider(ModelMap mm) throws Exception {
        mm.put("LIST_ITEM", new ProviderHandle().findAllAvailable());
        return "Admin.Provider";
    }

    @RequestMapping(value = "/Content", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView providerContent(ModelMap mm) throws Exception {
        mm.put("LIST_ITEM", new ProviderHandle().findAllAvailable());
        return new ModelAndView("Admin.Provider.Content");
    }

    @RequestMapping(value = "/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView viewInsert() {
        return new ModelAndView("Admin.Provider.ViewInsert");
    }

    @RequestMapping(value = "/Insert", method = RequestMethod.POST)
    @ResponseBody
    public Pair insert(@RequestBody Map mapProvider) {
        try {
            Provider provider = (Provider) new ProviderHandle().getObjectFromMap(mapProvider);

            new ProviderHandle().create(provider);
            return new Pair(1, Alert.createSuccessAlert("Thêm nhà cung cấp thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Thêm nhà cung cấp thất bại"));
        }
    }

    @RequestMapping(value = "/ViewEdit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewEdit(@PathVariable(value = "id") Integer id, ModelMap mm) throws Exception {
        mm.put("SELECTED_ITEM", new ProviderHandle().findById(id));
        return new ModelAndView("Admin.Provider.ViewEdit");
    }

    @RequestMapping(value = "/Edit", method = RequestMethod.POST)
    @ResponseBody
    public Pair Edit(@RequestBody Map mapProvider) {
        try {
            Provider provider = (Provider) new ProviderHandle().getObjectFromMap(mapProvider);
            new ProviderHandle().update(provider);
            return new Pair(1, Alert.createSuccessAlert("Sửa thông tin nhà cung cấp thành công!"));
        } catch (ConstraintViolationException cve) {
            cve.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Tài khoản đã tồn tại!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Sửa thông tin nhà cung cấp thất bại"));
        }
    }

    @RequestMapping(value = "/Delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Pair deleteAdminRole(@PathVariable(value = "id") Integer id) {
        try {
            new ProviderHandle().delete(id);
            return new Pair(1, Alert.createSuccessAlert("Xoá nhà cung cấp thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Xoá nhà cung cấp thất bại"));
        }
    }
}
