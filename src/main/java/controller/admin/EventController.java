package controller.admin;

import entity.Customer;
import entity.Event;
import entity.Field;
import handle.EventHandle;
import handle.FieldHandle;
import javafx.util.Pair;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import util.Alert;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/Ebms/Event")
public class EventController {

    @RequestMapping("")
    public String Event(ModelMap mm) throws Exception {
        List<Event> lst  = new EventHandle().findAllAvailable();
        for(Event dto : lst) {
            if (!StringUtils.isEmpty(dto.getFieldIDs())
                    && dto.getFieldIDs().length() > 0) {
                String[] arrFieldID = dto.getFieldIDs().split(",");
                List<String> list = Arrays.asList(arrFieldID);
                List<Integer> integerList = list.stream()
                        .map(s -> Integer.parseInt(s))
                        .collect(Collectors.toList());
                dto.setFieldIDs(new FieldHandle().getNameField(integerList));
            }
        }
        mm.put("LIST_ITEM",lst);
        return "Admin.Event";
    }

    @RequestMapping(value = "/Content", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView EventContent(ModelMap mm) throws Exception {
        List<Event> lst  = new EventHandle().findAllAvailable();
        for(Event dto : lst) {
            if (!StringUtils.isEmpty(dto.getFieldIDs())
                    && dto.getFieldIDs().length() > 0) {
                String[] arrFieldID = dto.getFieldIDs().split(",");
                List<String> list = Arrays.asList(arrFieldID);
                List<Integer> integerList = list.stream()
                        .map(s -> Integer.parseInt(s))
                        .collect(Collectors.toList());
                dto.setFieldIDs(new FieldHandle().getNameField(integerList));
            }
        }
        mm.put("LIST_ITEM",lst);
        return new ModelAndView("Admin.Event.Content");
    }

    @RequestMapping(value = "/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView viewInsert(ModelMap mm) throws Exception {
        mm.put("LIST_FIELD", new FieldHandle().findAll());
        return new ModelAndView("Admin.Event.ViewInsert");
    }

    @RequestMapping(value = "/Insert", method = RequestMethod.POST)
    @ResponseBody
    public Pair insert(@RequestBody Map mapEvent) {
        try {

            Event Event = (Event) new EventHandle().getObjectFromMap(mapEvent);
            if(Event.getNameAscii().length()>0){
                if(new EventHandle().checkNameAscii(Event.getNameAscii())==true){
                    new EventHandle().create(Event);
                    return new Pair(1, Alert.createSuccessAlert("Thêm sự kiện thành công!"));
                }
                return new Pair(0, Alert.createErrorAlert("Đường dẫn đã tồn tại!"));
            }
            return new Pair(0, Alert.createErrorAlert("Đường dẫn trống!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Thêm sự kiện thất bại"));
        }
    }

    @RequestMapping(value = "/ViewEdit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewEdit(@PathVariable(value = "id") Integer id, ModelMap mm) throws Exception {
        mm.put("LIST_FIELD", new FieldHandle().findAll());
        mm.put("SELECTED_ITEM", new EventHandle().findById(id));
        return new ModelAndView("Admin.Event.ViewEdit");
    }

    @RequestMapping(value = "/Edit", method = RequestMethod.POST)
    @ResponseBody
    public Pair Edit(@RequestBody Map mapEvent) {
        try {
            Event Event = (Event) new EventHandle().getObjectFromMap(mapEvent);
            new EventHandle().update(Event);
            return new Pair(1, Alert.createSuccessAlert("Sửa thông tin sự kiện thành công!"));
        } catch (ConstraintViolationException cve) {
            cve.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Tài khoản đã tồn tại!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Sửa thông tin sự kiện thất bại"));
        }
    }

    @RequestMapping(value = "/Delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Pair deleteAdminRole(@PathVariable(value = "id") Integer id) {
        try {
            new EventHandle().delete(id);
            return new Pair(1, Alert.createSuccessAlert("Xoá sự kiện thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Pair(0, Alert.createErrorAlert("Xoá sự kiện thất bại"));
        }
    }
}
