<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">          
            <form id="my-form" novalidate action="/Ebms/Address/Insert">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm địa chỉ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Tên địa chỉ</label>
                            <input name="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Link bản đồ</label>
                            <input id="linkMap"  name="googleMapUrl" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <iframe id="iframe"  src="" class="form-control" ></iframe>
                        </div>
                        <div class="form-group">
                            <label>Mô tả</label>
                            <input name="description" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Huỷ</button>
                    <button type="submit" id="btn-submit" class="btn btn-primary">Lưu</button>
                </div>      
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <script>


        $(document).ready(function () {
            $('#my-form').validate({
                submitHandler: function () {
                    var data = $('#my-form').serializeObject();
                    var url = $('#my-form').attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function (result) {
                            openAlert(result.value);
                            if (result.key) {
                                $('#myModal').modal('hide');
                                reloadAjaxContent();
                            }
                        },
                        error: function () {
                            errorAlert();
                        }
                    });
                }
            });
            $('#iframe').attr('style','display:none');
            $("#linkMap").on("change",function () {
                var link =  $("#linkMap").val();
                if(link !=null&& link !=""){
                    $('#iframe').attr('src', link);
                    $("#iframe").attr('style','display:');
                    $("#iframe").attr('style','height:200px');
                }else{
                    $("#iframe").attr('style','display:none');
                }
            });
        });
    </script>
</div>